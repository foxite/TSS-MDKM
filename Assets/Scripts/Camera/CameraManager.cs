﻿using UnityEngine;

namespace Game.CustomCamera
{
	public class CameraManager : MonoBehaviour
	{
		[SerializeField] private Transform m_playerTransform;
		[SerializeField] private float m_cameraHeight;
		[SerializeField] private float m_cameraZoffset;

		private void Update()
		{
			transform.position = new Vector3(m_playerTransform.position.x - m_cameraZoffset, m_cameraHeight, m_playerTransform.position.z);
		}

	}
}
