﻿using Game.Character;
using Game.flashlight;
using Game.Ui;
using Game.Util;
using Game.weapons;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Inventory
{
	[Serializable]
	public class InventoryManager : MonoBehaviour
	{
		public static InventoryManager Instance;
		
		public int Ammo { get; private set; }
		public int MaxAmmo => m_maxAmmo;
		public int Batteries { get; private set; }
		public FlashlightManager Flashlight { get; private set; }
		private int m_activeSlot;

		[SerializeField] private int m_maxAmmo;
		[SerializeField] private Transform m_weaponHoldingPosition;
		[SerializeField] private Transform m_flashlightHoldingPosition;
		[SerializeField] private Text m_BatteryCount;

		[Header("debug")]
		[SerializeField] private List<InventoryItem> m_inventoryItems = new List<InventoryItem>();

		private void Awake()
		{
			Instance = this;
			m_BatteryCount.text = "";
		}

		private void Update()
		{
			SwitchWeapon();
			GuiManager.Instance.HotbarOutline(m_activeSlot);
		}


		/// <summary>
		/// Add an item to the inventory
		/// </summary>
		/// <param name="_item">InventoryItem, the item to add</param>
		public void AddItem(InventoryItem _item)
		{
			if (_item == null)
				return;

			if (m_inventoryItems.Count != 4)
			{
				m_inventoryItems.Add(_item);
				_item.m_ItemEnabled = false;
			}
			else
			{
				InventoryItem _droppedGm = m_inventoryItems[m_activeSlot];
				StartCoroutine(EnableDroppedItemScript(_droppedGm));

				SetDroppedItemPosition();
				SwitchInvItems(_item);
			}
			FlashlightManager flashlight = _item.GetComponent<FlashlightManager>();
			if (flashlight != null)
				Flashlight = flashlight;
			ShowSelectedWeapon();
		}

		public void RemoveItem(InventoryItem _item)
		{
			for (int i = m_inventoryItems.Count; i < m_inventoryItems.Count; i++)
			{
				if (m_inventoryItems[i] == _item)
				{
					RemoveItem(i); // Can't simply call m_inventoryItems.Remove(_item) because we need the index

					//this fixes that you can select an empty slot if you remove a item in your hand/ infront of your hand becuase the list wil auto sort..........?
					if (1 > m_activeSlot)
					{
						m_activeSlot--;
					}

					break;
				}
			}
		}

		public void RemoveItem(int index)
		{
			if (m_inventoryItems[index].GetComponent<FlashlightManager>() == Flashlight)
				Flashlight = null;
			m_inventoryItems.RemoveAt(index);
			if (m_inventoryItems.Count != 0 && m_activeSlot <= m_inventoryItems.Count)
				m_activeSlot = m_inventoryItems.Count - 1;
		}

		/// <summary>
		/// switch the current holding item with a new item
		/// </summary>
		/// <param name="_item"></param>
		private void SwitchInvItems(InventoryItem _item)
		{
			m_inventoryItems[m_activeSlot] = _item;
		}
		/// <summary>
		/// set the pos and parrent of the dropped item
		/// </summary>
		private void SetDroppedItemPosition()
		{
			m_inventoryItems[m_activeSlot].transform.position = transform.position;
			m_inventoryItems[m_activeSlot].transform.SetParent(null);
		}
		/// <summary>
		/// enables the dropped item pickupscript after a while to prefent glitches
		/// </summary>
		/// <param name="_item"></param>
		/// <returns></returns>
		private IEnumerator EnableDroppedItemScript(InventoryItem _item)
		{
			yield return new WaitForSeconds(.1f);
			_item.m_ItemEnabled = true;

		}
		/// <summary>
		/// handles weapon switching
		/// </summary>
		private void SwitchWeapon()
		{
			if (m_inventoryItems.Count > 0)
			{ // Inventory contains at least 1 item
				if (Input.GetButtonDown("HotbarMovementLeft"))
				{
					if (m_activeSlot == 0)
					{
						m_activeSlot = m_inventoryItems.Count - 1;
						ShowSelectedWeapon();
					}
					else
					{
						m_activeSlot--;
						ShowSelectedWeapon();
					}
				}
				if (Input.GetButtonDown("HotbarMovementRight"))
				{
					if (m_activeSlot == m_inventoryItems.Count - 1)
					{
						m_activeSlot = 0;
						ShowSelectedWeapon();
					}
					else
					{
						m_activeSlot++;
						ShowSelectedWeapon();
					}
				}

				KeyCode invKeyPressed = Functions.GetFirstKey(KeyCode.Alpha1, KeyCode.Alpha4);
				if (invKeyPressed != KeyCode.None)
				{
					int tmpNewActiveSlot = invKeyPressed - KeyCode.Alpha1;
					if (m_inventoryItems.Count > tmpNewActiveSlot)
					{// Only switch slots if the selected one is not empty
						m_activeSlot = tmpNewActiveSlot;
						ShowSelectedWeapon();
					}
				}
			}
		}

		/// <summary>
		/// checks if the inventory is full
		/// </summary>
		/// <returns></returns>
		public bool CheckIfFull()
		{
			return m_inventoryItems.Count > 3;
		}

		/// <summary>
		/// shows the current weapon on the charachter and hides the other
		/// </summary>
		private void ShowSelectedWeapon()
		{

			if (m_inventoryItems.Count == 0)
				return;



			for (int i = 0; i < m_inventoryItems.Count; i++)
			{
				if (m_inventoryItems[i] == null)
					break;
				if (m_inventoryItems[i].m_ItemType != ItemType.gun && m_inventoryItems[i].m_ItemType != ItemType.flashlight)
					continue;
				else
					PlayerControl.Instance.m_itemRightHolding = PlayerRightHandItem.nothing;

				FlashlightManager flashlight = m_inventoryItems[i].GetComponent<FlashlightManager>();
				if (flashlight != null && ReferenceEquals(Flashlight, flashlight))
				{
					flashlight.transform.SetParent(m_flashlightHoldingPosition);
					flashlight.transform.localPosition = Vector3.zero;
					flashlight.transform.rotation = new Quaternion(0, 0, 0, 0);
					flashlight.m_isHoldingLight = true;
					PlayerControl.Instance.m_itemLeftHandHolding = PlayerLeftHandItem.Flashlight;

					//this is to remove the item/sprite from the hotbar because you are always holding the light so the hotbar in unnessecary for this item
					RemoveItem(i);
				}
				else if (i == m_activeSlot)
				{
					m_inventoryItems[m_activeSlot].gameObject.transform.SetParent(m_weaponHoldingPosition);
					m_inventoryItems[m_activeSlot].transform.localPosition = Vector3.zero;
					m_inventoryItems[m_activeSlot].transform.rotation = new Quaternion(0, 0, 0, 0);

					WeaponManager _weaponManager = m_inventoryItems[i].GetComponent<WeaponManager>();

					if (_weaponManager != null)
					{
						switch (_weaponManager.m_weaponType)
						{
						case WeaponType.Pistol:
							PlayerControl.Instance.m_itemRightHolding = PlayerRightHandItem.Pistol;
							break;
						}
						_weaponManager.m_enabled = true;
					}
				}
				else
				{
					WeaponManager _weaponManager = m_inventoryItems[i].GetComponent<WeaponManager>();

					if (_weaponManager != null)
						_weaponManager.m_enabled = false;

					m_inventoryItems[i].transform.position = new Vector3(999, 999, 999);
				}
			}
		}
		public void AddBatteries(int _ammount)
		{
			Batteries += _ammount;
			m_BatteryCount.text = Batteries.ToString();
		}
		public void RemoveBattery(int _ammount)
		{
			Batteries -= _ammount;
			m_BatteryCount.text = Batteries.ToString();
		}
		public void AddAmmo(int _amount)
		{
			Ammo += _amount;
		}
		public void RemoveAmmo(int _amount)
		{
			Ammo -= _amount;
		}

		public List<InventoryItem> GetInventoryList()
		{
			return m_inventoryItems;
		}

		public InventoryItem GetCurrentItem()
		{
			if (m_inventoryItems.Count == 0)
				return null;
			return m_inventoryItems[m_activeSlot];
		}

		public bool CheckIfHasItem(string _itemName)
		{
			foreach (var item in m_inventoryItems)
			{
				if (item.name == _itemName)
				{
					return true;
				}
			}
			return false;
		}
	}
}
