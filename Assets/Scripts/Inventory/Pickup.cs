﻿using System.Collections;
using System.Collections.Generic;
using Game.Character;
using Game.Ui;
using UnityEngine;

namespace Game.Inventory
{
	[RequireComponent(typeof(Collider))]
	public class Pickup : MonoBehaviour
	{
		public PickupType m_PickupType;
		public int m_Amount;

		private Notification m_PickupNotif = null;
		private bool m_CanPickup;
		private bool m_hasAllreadyPickedup;
		private void OnTriggerEnter(Collider other)
		{
			switch (m_PickupType)
			{
			case PickupType.Ammo:
				m_CanPickup = InventoryManager.Instance.Ammo < InventoryManager.Instance.MaxAmmo;
				break;
			case PickupType.Health:
				m_CanPickup = PlayerControl.Instance.GetHealth() < PlayerControl.Instance.GetMaxHealth();
				break;
			case PickupType.Armor:
				m_CanPickup = PlayerControl.Instance.GetArmor() < PlayerControl.Instance.GetMaxArmor();
				break;
			case PickupType.Batteries:
				m_CanPickup = true;
				break;
			default:
				m_CanPickup = false;
				break;
			}

			if (m_PickupNotif == null)
			{
				if (m_CanPickup)
				{
					m_PickupNotif = NotificationManager.GetInstance().PostNotification(-1.0f, "Press Y to pick up " + m_PickupType.ToString().ToLower(), Color.yellow);
				}
				else
				{
					m_PickupNotif = NotificationManager.GetInstance().PostNotification(-1f, "You have enough " + m_PickupType.ToString().ToLower() + ", you don't need any more.", Color.red);
				}
			}
		}

		private void OnTriggerStay(Collider other)
		{
			if (m_hasAllreadyPickedup)
				// Even tho the gameObject gets destroyed when picked up, the actual destruction will happen at the end of the current frame,
				//  and this function will be called twice because the player has two colliders.
				return;


			if (other.transform.CompareTag(Tags.Player.ToString()))
			{
				if (m_CanPickup)
				{
					if (Input.GetButtonDown("ItemPickup"))
					{
						m_hasAllreadyPickedup = true;
						// Pick up item
						switch (m_PickupType)
						{
						case PickupType.Ammo:
							InventoryManager.Instance.AddAmmo(m_Amount);
							break;
						case PickupType.Health:
							PlayerControl.Instance.GiveHealth(m_Amount);
							break;
						case PickupType.Armor:
							PlayerControl.Instance.GiveArmor(m_Amount);
							break;
						case PickupType.Batteries:
							InventoryManager.Instance.AddBatteries(m_Amount);
							break;
						}
						if (m_PickupNotif != null)
						{
							NotificationManager.GetInstance().RemoveNotification(m_PickupNotif);
							m_PickupNotif = null;
						}
						Destroy(gameObject);
					}
				}
			}
		}

		private void OnTriggerExit(Collider other)
		{
			m_CanPickup = false;

			if (m_PickupNotif != null)
			{
				NotificationManager.GetInstance().RemoveNotification(m_PickupNotif);
				m_PickupNotif = null;
			}
		}
	}

	public enum PickupType { Health, Armor, Batteries, Ammo }
}
