﻿using UnityEngine;
using Game.Ui;
using System;
using Game.Character;
using NaughtyAttributes;

namespace Game.Inventory
{
	public enum ItemType { gun, keycard, flashlight }
	[RequireComponent(typeof(Collider)), Serializable]
	public class InventoryItem : MonoBehaviour
	{
		/* [HideInInspector]*/
		public bool m_ItemEnabled = true;
		public Sprite m_HotbarIcon;
		public string m_DisplayName;
		public ItemType m_ItemType;

		[SerializeField, Header("This should be equal to the prefab's file name")]
		private string m_InternalName; // For saving items. Do not change in instances; only change in the prefab.

		[SerializeField, Header("note: this required a itemHighlight shader material!")] public bool m_enableHighlighting;
		
		[ShowIf("m_enableHighlighting"), SerializeField] private Material m_highlightMaterial;
		[ShowIf("m_enableHighlighting"), SerializeField] private Material m_defaultMaterial;

		private Notification m_PickupNotif = null;
		private GameObject m_player;
		private MeshRenderer m_renderer;

		private void Start()
		{
			m_renderer = GetComponent<MeshRenderer>();
			m_player = PlayerControl.Instance.gameObject;
		}
		
		private void OnTriggerEnter(Collider other)
		{
			if (other.tag != "Player")
				return;
			if (m_enableHighlighting)
				m_renderer.material = m_highlightMaterial;
		}

		private void OnTriggerExit(Collider other)
		{
			if (other.tag != "Player")
				return;
			if (m_enableHighlighting)
				m_renderer.material = m_defaultMaterial;
			if (m_PickupNotif == null)
				return;
			NotificationManager.GetInstance().RemoveNotification(m_PickupNotif);
		}

		private void OnTriggerStay(Collider other)
		{
			if (!m_ItemEnabled)
				return;

			if (!other.transform.CompareTag(Tags.Player.ToString()))
				return;


			if (m_PickupNotif == null)
			{
				m_PickupNotif = NotificationManager.GetInstance().PostNotification(-1.0f, "Press Y to pickup: " + m_DisplayName, Color.yellow);
			}
			if (Input.GetButtonUp("ItemPickup"))
			{
				InventoryManager _invManager = other.GetComponent<InventoryManager>();
				if (_invManager == null)
					return;
				
				transform.position = new Vector3(999, 999, 999); // Object pulling

				_invManager.AddItem(this);
				NotificationManager.GetInstance().RemoveNotification(m_PickupNotif);
			}
		}
		public string GetInternalName()
		{
			return m_InternalName;
		}
	}
}