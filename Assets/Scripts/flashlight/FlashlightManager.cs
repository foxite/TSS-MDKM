﻿using System.Collections;
using Game.Inventory;
using Game.Ui;
using UnityEngine;

namespace Game.flashlight
{
	public class FlashlightManager : MonoBehaviour
	{

		private InventoryItem m_inventoryItem;
		private BatteryBar m_batteryBar;
		private Notification m_noBatterieLeftNotification;


		[HideInInspector] public float m_batterieLifeTimer;
		private bool m_isFlashlightAxisInUse = false;

		public bool m_isHoldingLight;
		public float m_batterieLifeInSeconds;
		[SerializeField] private Light m_light;
		[SerializeField] private float m_flickringAmmount = 10f;


		[Header("debug")]
		public bool m_enabled;
		[SerializeField] private bool m_flashlightOn;
		[SerializeField] private bool m_hasBatterie;

		private void Awake()
		{
			m_batteryBar = FindObjectOfType<BatteryBar>();
			m_batteryBar.enabled = false;
		}
		private void Start()
		{
			m_inventoryItem = GetComponent<InventoryItem>();
		}
		private void Update()
		{

			RaycastFlashlightRange();

			if (!m_isHoldingLight)
			{
				m_batteryBar.enabled = false;
				return;
			}

			if (m_hasBatterie)
				m_batteryBar.enabled = true;


			m_enabled = !m_inventoryItem.m_ItemEnabled;
			m_light.enabled = m_flashlightOn;

			if (!m_enabled)
			{
				m_flashlightOn = false;
				return;
			}
			if (!Mathf.Approximately(Input.GetAxisRaw("Flashlight"), 0.0f))
			{
				if (m_isFlashlightAxisInUse == false)
				{
					GetComponent<AudioSource>().Play();
					m_isFlashlightAxisInUse = true;
					m_flashlightOn = !m_flashlightOn;
					AddBatteryToLight();
				}
			}
			else
			{
				m_isFlashlightAxisInUse = false;
			}

			if (!m_flashlightOn)
				return;

			CheckBattery();

			//if (!m_isFlickring)
			//	if (m_batterieLifeTimer > m_batterieLifeInSeconds - 10)
			//	{
			//		m_isFlickring = true;
			//		StartCoroutine(FlickerLight());
			//	}
		}
		private IEnumerator FlickerLight()
		{

			for (int i = 0; i < m_flickringAmmount; i++)
			{

				if (!m_flashlightOn)
					continue;

				yield return new WaitForSeconds(UnityEngine.Random.Range(.1f, 1f));

				if (m_flashlightOn)
					m_light.enabled = !m_light.enabled;
				else
					yield return null;

			}
		}

		private void AddBatteryToLight()
		{
			if (!m_hasBatterie)
			{
				if (InventoryManager.Instance.Batteries > 0)
				{
					InventoryManager.Instance.RemoveBattery(1);
					m_batterieLifeTimer = 0;

					m_hasBatterie = true;
				}
				else
				{
					if (m_noBatterieLeftNotification == null)
						m_noBatterieLeftNotification = NotificationManager.GetInstance().PostNotification(1, "You don't have any batteries left!", Color.red);

					m_flashlightOn = false;
				}
			}
		}


		private void CheckBattery()
		{
			if (m_hasBatterie)
			{
				m_batterieLifeTimer += Time.deltaTime;

				if (m_batterieLifeTimer >= m_batterieLifeInSeconds)
				{
					m_batterieLifeTimer = m_batterieLifeInSeconds;
					m_hasBatterie = false;
					m_flashlightOn = false;
				}
			}
		}

		public float GetMaxBatteryLife()
		{
			return m_batterieLifeInSeconds;
		}

		public float GetBatteryLifeLeft()
		{
			return m_batterieLifeInSeconds - m_batterieLifeTimer;
		}
		private void RaycastFlashlightRange()
		{
			if (!m_flashlightOn || !m_enabled)
				return;

			Ray _ray = new Ray(m_light.transform.position, m_light.transform.forward);
			RaycastHit _hit;


			if (Physics.Raycast(_ray, out _hit))
			{
				//print(_hit.distance);
				//m_light.range = _hit.distance;
				//int _defaultIntensity = 3;

				//m_light.intensity = _defaultIntensity - (0.1f * _hit.distance);
			}
		}
	}
}
