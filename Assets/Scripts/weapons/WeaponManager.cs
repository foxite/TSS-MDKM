﻿using UnityEngine;
using Game.Inventory;
using Game.Ui;
using Game.Character;
using Game.Data;
using System;

namespace Game.weapons
{
    public enum WeaponType { Pistol }
	public static class WeaponTypeFunctions {
		public static WeaponType FromString(string str) {
			switch (str) {
			case "Pistol": return WeaponType.Pistol;
			default: throw new ArgumentException(str + " is not a valid WeaponType.");
			}
		}
	}

    public class WeaponManager : MonoBehaviour
    {

        [SerializeField] private int m_magSize;
        [SerializeField] private int m_roundsPerMinut;
        [SerializeField] private float m_reloadTime;
        [Space, SerializeField] private Transform m_bulletSpawningPos;
        [SerializeField] private GameObject m_bullet;
        [SerializeField] private bool m_isAutomatic;
        public WeaponType m_weaponType;

        [Header("debug, do not type under this")]
        public int m_ammoInMag;
        [SerializeField] private bool m_isReloading;
        [SerializeField] private float m_roundsPerSeconds;
        [Space, SerializeField] private AudioClip m_shotSound;
        [SerializeField] private AudioClip m_reloadSound;
        [SerializeField] private AudioClip m_emptySound;
        public bool m_enabled;

        private float m_RpmTimer;
        private float m_reloadingTimer;
        private InventoryItem m_inventoryItem;
        private bool m_triggerIsDown;
        private Notification m_pressXtoReloadNotification;
        private bool m_hasShotWithCurrentTriggerInput;
        private void Start()
        {
            m_inventoryItem = GetComponent<InventoryItem>();
        }

        private void Update()
        {

            if (PlayerControl.Instance.m_actionState == PlayerActionState.Dead)
                return;


            if (!m_triggerIsDown)
            {
                if (Input.GetAxis("Shoot") > .9f)
                    m_triggerIsDown = true;
            }
            if (m_triggerIsDown)
            {
                if (Input.GetAxis("Shoot") < .1f  ) 
                {
                    m_hasShotWithCurrentTriggerInput = false;
                    m_triggerIsDown = false;
                }
            }

            if (Input.GetMouseButtonUp(0))
                m_hasShotWithCurrentTriggerInput = false;

            //recals the rps
            m_roundsPerSeconds = 1f / (m_roundsPerMinut / 60f);
            //checks if the gun has an inventory item script so this can be disables if the gun is droppen
            if (m_inventoryItem != null)
            {
                if (m_inventoryItem.m_ItemEnabled)
                    return;
            }
            if (!m_enabled)
                return;

            m_RpmTimer += Time.deltaTime;

            //handles the reloading + reloading delay
            if (m_isReloading)
            {
                m_reloadingTimer += Time.deltaTime;

                if (m_reloadingTimer >= m_reloadTime)
                {
                    m_reloadingTimer = 0;
                    RemoveBulletsFromMag(m_ammoInMag);
                    AddBulletsToMag(m_magSize);
                    m_isReloading = false;
					PlayerControl.Instance.m_actionState = PlayerActionState.Nothing;

                }
                return;
            }

            //reload btn
            if (Input.GetButtonDown("Reload"))
            {
                if (InventoryManager.Instance.Ammo == 0)
                {
                    NotificationManager.GetInstance().PostNotification(1, "Not enough ammo!", Color.red);
                    return;
                }
				PlayerControl.Instance.m_actionState = PlayerActionState.Reloading;

                playsfx(m_reloadSound);

                m_isReloading = true;
            }
            //checks ammo in mag


            if (m_ammoInMag <= 0)
            {
                if (m_triggerIsDown || Input.GetMouseButtonDown(0))
                {
                    playsfx(m_emptySound);

                    if (m_pressXtoReloadNotification == null)
                        m_pressXtoReloadNotification = NotificationManager.GetInstance().PostNotification(1, "Press X to reload", Color.green);

                    return;
                }

                return;

            }


            //shoot btn
            if (m_isAutomatic)
            {

                if (Input.GetAxis("Shoot") > .9f || Input.GetMouseButton(0))
                {
                    if (m_RpmTimer >= m_roundsPerSeconds)
                    {
                        m_RpmTimer = 0;
                        Shoot();
                    }
                }
                else if (!m_isReloading)
                {
					PlayerControl.Instance.m_actionState = PlayerActionState.Nothing;
                }
            }
            else
            {
                if (m_triggerIsDown )
                {
                    if (m_RpmTimer >= m_roundsPerSeconds && !m_hasShotWithCurrentTriggerInput)
                    {
                        m_hasShotWithCurrentTriggerInput = true;
                        m_RpmTimer = 0;
                        Shoot();
                    }
                }
                else if (!m_isReloading)
                {
					PlayerControl.Instance.m_actionState = PlayerActionState.Nothing;
                }

                if (Input.GetMouseButton(0))
                {
                    if (m_RpmTimer >= m_roundsPerSeconds && !m_hasShotWithCurrentTriggerInput)
                    {
                        m_hasShotWithCurrentTriggerInput = true;
                        m_RpmTimer = 0;
                        Shoot();
                    }
                }
                else if (!m_isReloading)
                {
					PlayerControl.Instance.m_actionState = PlayerActionState.Nothing;
                }
            }

        }

        private void playsfx(AudioClip _sfx)
        {
            AudioSource _audioSource = GetComponent<AudioSource>();
            _audioSource.clip = _sfx;
            _audioSource.Play();
        }

        /// <summary>
        /// removes the bullets from the  mag and add them back to the inv
        /// </summary>
        /// <param name="_ammoInMag"></param>
        private void RemoveBulletsFromMag(int _ammoInMag)
        {
            InventoryManager.Instance.AddAmmo(_ammoInMag);
            m_ammoInMag = 0;
        }
        /// <summary>
        /// adds bullet in the mag and removes them from inv
        /// </summary>
        /// <param name="_magSize"></param>
        private void AddBulletsToMag(int _magSize)
        {
            if (InventoryManager.Instance.Ammo > m_magSize)
            {
                //sets the ammo in mag to the mag mag size
                m_ammoInMag = _magSize;
                //removes ammo from inv
                InventoryManager.Instance.RemoveAmmo(_magSize);
            }
            //checks if theres atleast more than 1 bullet in inv
            else if (InventoryManager.Instance.Ammo > 0)
            {
                //puts the ammount of ammo from the inv to the mag
                m_ammoInMag = InventoryManager.Instance.Ammo;
                //removes the ammo from inventory
                InventoryManager.Instance.RemoveAmmo(InventoryManager.Instance.Ammo);
            }
            else
            {
                NotificationManager.GetInstance().PostNotification(1, "Not enough ammo", Color.green);
            }
        }
        /// <summary>
        /// shoots the gun
        /// </summary>
        private void Shoot()
        {
			PlayerControl.Instance.m_actionState = PlayerActionState.Shoot;
            DataManager.Instance.m_bulletsFired++;

            m_ammoInMag--;
            GameObject _ga = Instantiate(m_bullet, m_bulletSpawningPos.position, InventoryManager.Instance.gameObject.transform.localRotation);
            _ga.transform.localRotation = InventoryManager.Instance.gameObject.transform.localRotation;
            AudioSource _as = _ga.GetComponent<AudioSource>();
            _as.clip = m_shotSound;
            _as.Play();


        }
    }
}
