﻿using Game.Inventory;
using UnityEngine;

namespace Game.weapons {
	[RequireComponent(typeof(Rigidbody), typeof(Collider))]
	public class Bullet : MonoBehaviour {
		[SerializeField] private float m_velocity;

		private Rigidbody m_rb;
		private Collider m_collider;
        public bool m_isAlive = true;
        public GameObject m_owner;

        private bool m_enabled = true;
		private void Start() {
			m_rb = GetComponent<Rigidbody>();
			m_collider = GetComponent<Collider>();

            m_collider.isTrigger = true;
			m_rb.useGravity = false;

			m_rb.AddRelativeForce(Vector3.forward * m_velocity);

		}
		private void OnTriggerEnter(Collider other) {
            if (!m_enabled)
                return;

            this.GetComponent<Renderer>().enabled = false;
            this.GetComponent<MeshRenderer>().enabled = false;
            this.GetComponent<Light>().enabled = false;
            m_isAlive = false;
            this.m_enabled = false;
            Destroy(this.gameObject,1);
		}
	}
}