﻿using Game.Character;
using UnityEngine;

namespace Game.Pause
{

    public class PauseManager : MonoBehaviour
    {
		public static PauseManager Instance { get; private set; }
        public bool m_gamePaused;

        [SerializeField] private GameObject m_pausePannel;

		private void Awake()
		{
			Instance = this;
		}

		private void Update()
        {
            if (Input.GetButtonDown("Pause"))
            {
                SwitchPauseStatus();
            }
        }

        public void SwitchPauseStatus()
        {
            m_gamePaused = !m_gamePaused;
            m_pausePannel.SetActive(m_gamePaused);
			PlayerControl.Instance.enabled = !m_gamePaused;
            if (m_gamePaused)
                Time.timeScale = 0;
            else
                Time.timeScale = 1;
        }
    }
}
