﻿using UnityEngine;

namespace Game.Lightning
{
    public class LightFlickerEffect : MonoBehaviour
    {
		[SerializeField] private bool m_UseEmision;
		[SerializeField] private bool m_UseLight;
		[SerializeField, Header("Chance that the light will flip in a particular frame.")]
		private float m_FlipRate = 0.01f;

        private Light m_light;
		private Renderer m_renderer;

        private void Start()
        {
            m_light = GetComponent<Light>();

			if(m_light == null)
				m_light = GetComponentInChildren<Light>();

			m_renderer = GetComponent<Renderer>();
        }
        private void FixedUpdate()
        {
            float randomValue = Random.value;

            if (randomValue < m_FlipRate)
            {
				if (m_UseEmision)
				{
					if (m_renderer.material.IsKeywordEnabled("_EMISSION"))
					{
						m_renderer.material.DisableKeyword("_EMISSION");
					}
					else
					{
						m_renderer.material.EnableKeyword("_EMISSION");
 					}
				}
				if(m_UseLight)
				{
					m_light.enabled = !m_light.enabled;
				}
            }
        }

    }
}