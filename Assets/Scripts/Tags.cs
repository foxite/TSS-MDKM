﻿namespace Game
{
    public enum Tags
    {
        Player,
        Enemy,
        floor,
        floorButIgnoredByAi,
        Door,
		Room1Floor,
		Room2Floor,
		Room3Floor,
		Room4Floor
	}
}
