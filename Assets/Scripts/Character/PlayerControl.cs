﻿using UnityEngine;
using System.Collections;
using Game.Interfaces;
using Game.weapons;
using Game.Ui;
using Game.Data;
using UnityEngine.SceneManagement;

namespace Game.Character
{
	public enum PlayerMovementState { Idle, Walking }
	public enum PlayerActionState { Nothing, Shoot, Reloading, Dead }
	public enum PlayerRightHandItem { nothing, Pistol }
	public enum PlayerLeftHandItem { nothing, Flashlight }

	[RequireComponent(typeof(Rigidbody))]
	public class PlayerControl : HealthAndArmor
	{
		public static PlayerControl Instance { get; private set; }

		private Rigidbody m_rb;
		private AudioSource m_audioSource;
		private GameObject m_bulletOwner;
		private Vector3 m_mousePrevious; // Used to calculate the delta mouse position
		private float m_inputHorizontal;
		private float m_inputVertical;
		private float m_inputCameraHorizontal;
		private float m_inputCameraVertical;
		private float m_StepCycle;
		private float m_NextStep;
		private bool m_usingMouse; // true if using mouse to look, false if using controller

		[SerializeField] private float m_StepInterval, m_walkSpeed, m_rotateSpeed, m_maxHealth, m_health, m_armor, m_maxArmor;
		[Header("debug, do not touch this")]
		[SerializeField] private bool m_freezePlayer;
		[SerializeField] private float m_freezePlayerTime;
		[SerializeField] private AudioClip[] m_FootstepSounds;

		public PlayerMovementState m_walkingState;
		public PlayerActionState m_actionState;
		public PlayerRightHandItem m_itemRightHolding;
		public PlayerLeftHandItem m_itemLeftHandHolding;

		private void Awake()
		{
			Instance = this;
		}

		private void Start()
		{
			m_rb = GetComponent<Rigidbody>();
			m_audioSource = GetComponent<AudioSource>();
			m_mousePrevious = Input.mousePosition;
		}

		private void Update()
		{

			if (m_freezePlayer)
				return;

			GetInput();
			RotateCharacter();
			ProgressStepCycle(m_walkSpeed);
			HealthManager();
		}

		private void HealthManager()
		{
			if (m_health <= 0)
			{
				m_actionState = PlayerActionState.Dead;
				DataManager.Instance.AddKilledBy(m_bulletOwner.name);

				if (PannelFader.Instance != null)
					PannelFader.Instance.FadeToNextScene("GameOver", false);
				else
					SceneManager.LoadScene("GameOver");
			}
		}

		private void ProgressStepCycle(float speed)
		{
			//thanks unity standard player script
			if (m_rb.velocity.sqrMagnitude > 0 && m_walkingState == PlayerMovementState.Walking)
			{
				m_StepCycle += (m_rb.velocity.magnitude + speed) * Time.deltaTime;
			}

			if (m_StepCycle <= m_NextStep)
			{
				return;
			}

			m_NextStep = m_StepCycle + m_StepInterval;

			PlayFootStepAudio();
		}
		private void RotateCharacter()
		{
			if (m_usingMouse)
			{
				Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				RaycastHit _hit;
				if (Physics.Raycast(ray, out _hit))
				{
					Vector3 targetPoint = _hit.point;
					targetPoint.y = transform.position.y;

					Quaternion targetRotation = Quaternion.LookRotation(targetPoint - transform.position);

					transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, m_rotateSpeed * Time.deltaTime);
				}
			}
			else
			{
				if (!(Mathf.Approximately(m_inputCameraHorizontal, 0.0f) && Mathf.Approximately(m_inputCameraVertical, 0.0f)))
				{
					Vector3 _dir = new Vector3(transform.position.x + -m_inputCameraVertical, transform.position.y, transform.position.z + -m_inputCameraHorizontal);
					Vector3 _localDir = transform.localPosition + transform.forward;
					Vector3 _newDir = Vector3.Slerp(_localDir, _dir, m_rotateSpeed);
					transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(_newDir - transform.position), m_rotateSpeed * Time.deltaTime);
				}
			}
		}


		private void GetInput()
		{
		

			m_inputHorizontal = Input.GetAxis("MoveHorizontal");
			m_inputVertical = Input.GetAxis("MoveVertical");
			m_inputCameraHorizontal = Input.GetAxis("LookHorizontal");
			m_inputCameraVertical = Input.GetAxis("LookVertical");
			if (!(Mathf.Approximately(m_inputCameraHorizontal, 0.0f) && Mathf.Approximately(m_inputCameraVertical, 0.0f)))
			{
				m_usingMouse = false;
			}
			else if (m_mousePrevious != Input.mousePosition)
			{
				m_usingMouse = true;
			}
			m_mousePrevious = Input.mousePosition;
		}

		private void FixedUpdate()
		{

			if (m_freezePlayer)
				return;


			if (m_actionState == PlayerActionState.Dead)
				return;

			if (m_rb == null)
				return;

			if (m_inputCameraHorizontal != 0)
			{
				RotateCharacter();
			}

			m_rb.AddForce((Vector3.right * m_inputVertical) * m_walkSpeed);
			m_rb.AddForce((Vector3.forward * -m_inputHorizontal) * m_walkSpeed);

			if (m_actionState != PlayerActionState.Dead)
			{
				if (m_rb.velocity.sqrMagnitude > 0.01f)
				{
					m_walkingState = PlayerMovementState.Walking;
				}
				else
				{
					m_walkingState = PlayerMovementState.Idle;
				}
			}
		}
		private void PlayFootStepAudio()
		{
			//thanks unity
			if (m_walkingState == PlayerMovementState.Walking)
			{
				int n = Random.Range(1, m_FootstepSounds.Length);
				m_audioSource.clip = m_FootstepSounds[n];
				m_audioSource.PlayOneShot(m_audioSource.clip);
				m_FootstepSounds[n] = m_FootstepSounds[0];
				m_FootstepSounds[0] = m_audioSource.clip;
			}
		}
		public void FreezePlayer(float _time)
		{
			m_freezePlayer = true;
			StartCoroutine(FreezeTimer(_time));
		}
		public void FreezePlayer()
		{
			m_freezePlayer = true;
			StartCoroutine(FreezeTimer(99999));
		}
		private IEnumerator FreezeTimer(float _time)
		{
			while (true)
			{
				m_freezePlayerTime += Time.deltaTime;
				yield return new WaitForEndOfFrame();
				if (m_freezePlayerTime >= _time)
				{
					m_freezePlayerTime = 0;
					m_freezePlayer = false;
					break;
				}
			}
		}

		public override float GetMaxHealth()
		{
			return m_maxHealth;
		}

		public override float GetHealth()
		{
			return m_health;
		}

		public override float GetArmor()
		{
			return m_armor;
		}

		public override float GetMaxArmor()
		{
			return m_maxArmor;
		}

		public override void SetArmor(float armor)
		{
			if (!IsInvincible())
				m_armor = armor;
		}

		public override void SetHealth(float health)
		{
			if (!IsInvincible())
				m_health = health;
		}

		public override void SetMaxArmor(float armor)
		{
			if (!IsInvincible())
				m_maxArmor = armor;
		}

		public override void SetMaxHealth(float health)
		{
			if (!IsInvincible())
				m_maxHealth = health;
		}

		public override void GiveArmor(float armor)
		{
			if (!IsInvincible())
			{
				m_armor += armor;
				if (m_armor > m_maxArmor)
					m_armor = m_maxArmor;
			}
		}

		public override void GiveHealth(float health)
		{
			if (!IsInvincible())
			{
				m_health += health;
				if (m_health > m_maxHealth)
					m_health = m_maxHealth;
			}
		}

		public override void DealDamage(float damage)
		{
			if (!IsInvincible())
			{
				m_armor -= damage;
				if (m_armor < 0)
				{
					m_health += m_armor; // m_armor is negative so adding it to m_health decreases m_heath
					m_armor = 0;
				}
			}
		}

		public override bool IsInvincible()
		{
			return m_freezePlayer;
		}
		
		private void OnTriggerEnter(Collider other)
		{
			Bullet _bullet = other.GetComponent<Bullet>();

			if (_bullet == null)
				return;

			if (_bullet.m_owner == this.gameObject)
				return;

			if (_bullet.m_isAlive == false)
				return;

			//this is to save who last hitted you, for the "killed by" text in dead screen
			m_bulletOwner = _bullet.m_owner;

			DealDamage(10);
		}
	}
}
