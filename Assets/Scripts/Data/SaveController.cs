﻿using System;
using System.IO;
using Game.Character;
using Game.flashlight;
using Game.Interfaces;
using Game.Inventory;
using Game.Keycards;
using Game.Util;
using Game.weapons;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Game.Data {
	public class SaveController : MonoBehaviour {
		public static SaveController Instance { get; private set; }

		public string SavedLevelName { get; private set; }
		private JObject m_State;

		private void Awake()
		{
			Instance = this;
		}

		/// <summary>
		/// Load the save file. Do this before calling Initialize().
		/// </summary>
		public void LoadGame() {
			try {
				m_State = JObject.Parse(File.ReadAllText(Application.persistentDataPath + "/savegame.dat"));
				SavedLevelName = m_State.GetValue("currentLevel").ToString();
			} catch (Exception e) {
				Debug.LogError("Loading failed!");
				Debug.LogError(e);
			}
		}

		/// <summary>
		/// Will change the currently loaded level according to the saved data. Make sure you call LoadGame() before this. This is to allow you to check what level was saved before continuing.
		/// </summary>
		public void Initialize() {
			if (m_State == null) {
				throw new InvalidOperationException("Initialize() was called before LoadGame(), or loading failed.");
			}

			JToken playerHP, playerArmor, ammo, batteries, items;
			if (m_State.TryGetValue("playerHP",    out playerHP   ) &&
				m_State.TryGetValue("playerArmor", out playerArmor) &&
				m_State.TryGetValue("ammo",        out ammo       ) &&
				m_State.TryGetValue("batteries",   out batteries  ) &&
				m_State.TryGetValue("inventory",   out items      )) {

				PlayerControl.Instance.SetHealth(playerHP   .ToObject<float>());
				PlayerControl.Instance.SetArmor (playerArmor.ToObject<float>());

				InventoryManager.Instance.AddAmmo     (ammo     .ToObject<int>());
				InventoryManager.Instance.AddBatteries(batteries.ToObject<int>());

				foreach (JObject itemObj in items as JArray) {
					GameObject itemGO = Instantiate(Resources.Load<GameObject>("Items/" + itemObj.GetValue("name")));
					string type = itemObj.GetValue("type").ToObject<string>();
					switch (type) {
					case "flashlight":
						itemGO.GetComponent<FlashlightManager>().m_batterieLifeTimer = itemObj.GetValue("batteryLifeTimer").ToObject<float>();
						break;
					case "weapon":
						WeaponManager wm = itemGO.GetComponent<WeaponManager>();
						wm.m_weaponType = WeaponTypeFunctions.FromString(itemObj.GetValue("weaponType").ToObject<string>());
						wm.m_ammoInMag = itemObj.GetValue("ammoInMag").ToObject<int>();
						break;
					case "keycard":
						Keycard kc = itemGO.GetComponent<Keycard>();
						kc.m_keycardColorCode = KeycardColorCodeFunctions.FromString(itemObj.GetValue("color").ToObject<string>());
						kc.m_specialID = itemObj.GetValue("specialID").ToObject<int>();
						break;
					default: throw new Exception("Saved data contains an invalid item of type " + type); // Can't use FileFormatException, because you need to reference WindowsBase and that requires .NET 3.5.
					}
					InventoryManager.Instance.AddItem(itemGO.GetComponent<InventoryItem>());
				}
			}
		}

		public void SaveGame(string level, HealthAndArmor playerHealth, InventoryManager inventory) {
			// Creates the basic save file. This is easy.
			JObject saveData = new JObject {
				{ "currentLevel", level },
				{ "playerHP", playerHealth.GetHealth() },
				{ "playerArmor", playerHealth.GetArmor() },
				{ "ammo", inventory.Ammo },
				{ "batteries", inventory.Batteries }
			};

			// Saving items is more complicated. We'll need an array of objects that do not have the same structure, as we might need to store a flashlight, or a keycard.
			JArray itemArray = new JArray();

			foreach (InventoryItem item in inventory.GetInventoryList()) {
				JObject itemObj = new JObject();
				// Our object always contains a "name" and a "type" key. The type key is not added here because its value is more complicated to determine.
				itemObj.Add("name", item.GetInternalName());

				// We'll need to go through each item and add its data, which is stored in components.
				// We need to see if a component is present, and if it is, then add its data to the json. If it's not, then check the next component.
				// In this way we check for FlashlightManager, WeaponManager, and Keycard.
				Component comp = Functions.GetBehaviourFromObject(new Type[] { typeof(FlashlightManager), typeof(WeaponManager), typeof(Keycard) }, item.gameObject);

				if (comp is FlashlightManager)
				{
					FlashlightManager fm = (FlashlightManager) comp;
					itemObj.Add("type", "flashlight");
					itemObj.Add("batteryLifeTimer", fm.m_batterieLifeTimer);
				}
				else if (comp is WeaponManager)
				{
					WeaponManager wm = (WeaponManager) comp;
					itemObj.Add("type", "weapon");
					itemObj.Add("weaponType", wm.m_weaponType.ToString());
					itemObj.Add("ammoInMag", wm.m_ammoInMag);
				}
				else if (comp is Keycard)
				{
					Keycard kc = (Keycard) comp;
					itemObj.Add("type", "keycard");
					itemObj.Add("color", kc.m_keycardColorCode.ToString());
					itemObj.Add("specialID", kc.m_specialID);
				}
				else if (comp != null)
				{
					Debug.LogError("GetBehaviourFromObject for an item returned an component with this unknown type: " + comp.GetType());
				}
				
				// Lastly we'll add the object we've created to the array.
				itemArray.Add(itemObj);
			}
			// Finally, we'll add our array to the save data.
			saveData.Add("inventory", itemArray);

			// And write it.
			File.WriteAllText(Application.persistentDataPath + "/savegame.dat", saveData.ToString(Newtonsoft.Json.Formatting.None));
		}
	}
}
