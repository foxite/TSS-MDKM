﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace Game.Data
{

public class SetDataText : MonoBehaviour {
    [SerializeField] private List<Text> m_text = new List<Text>();
    
    private void Start()
    {
            Data _data = JsonUtility.FromJson<Data>(PlayerPrefs.GetString("Data"));
            m_text[0].text = _data.m_timesPlayed.ToString();
            m_text[1].text = _data.m_enemysKilled.ToString();
            m_text[2].text = _data.m_bulletsFired.ToString();
            m_text[3].text = _data.m_score.ToString();
            m_text[4].text = _data.m_killedBy;
        }
}
}
