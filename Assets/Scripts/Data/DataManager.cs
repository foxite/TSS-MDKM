﻿using System;
using UnityEngine;

namespace Game.Data
{
    public class DataManager : MonoBehaviour
    {
		public static DataManager Instance { get; private set; }

        [HideInInspector] public int m_timesPlayed;
        [HideInInspector] public int m_enemysKilled;
        [HideInInspector] public int m_bulletsFired;
        [HideInInspector] public string m_killedBy;
        [HideInInspector] public int m_score;
        private void Awake()
		{
			Instance = this;

			LoadData();
            ResetData();
            m_timesPlayed++;
        }

        private void ResetData()
        {
            m_bulletsFired = 0;
            m_killedBy = "";
            m_enemysKilled = 0;
            m_score = 0;
        }

        private void OnDestroy()
        {
            SaveData();
        }

        private void SaveData()
        {
            string _json = JsonUtility.ToJson(this, true);
            PlayerPrefs.SetString("Data", _json);
        }

        private void LoadData()
        {
            Data _data = JsonUtility.FromJson<Data>(PlayerPrefs.GetString("Data"));

            if (_data == null)
                return;

            m_timesPlayed = _data.m_timesPlayed;
            m_enemysKilled = _data.m_enemysKilled;
            m_bulletsFired = _data.m_bulletsFired;
            m_killedBy = _data.m_killedBy;
            m_score = _data.m_score;
        }
        public void AddKilledBy(string _enemyName)
        {
            m_killedBy = _enemyName;
        }
    }

    [Serializable]
    public class Data
    {
        public int m_timesPlayed;
        public int m_enemysKilled;
        public int m_bulletsFired;
        public string m_killedBy;
        public int m_score;
    }
}
