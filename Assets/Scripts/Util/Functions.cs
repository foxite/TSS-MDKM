﻿using System;
using UnityEngine;

namespace Game.Util {
	/// <summary>
	/// Assortment of reusable functions.
	/// </summary>
	public static class Functions {
		/// <summary>
		/// Returns the first key in the given range that is is pressed on the keyboard. This uses GetKeyDown.
		/// </summary>
		/// <param name="to">Inclusive.</param>
		/// <returns>KeyCode.None if none is being pressed.</returns>
		public static KeyCode GetFirstKeyDown(KeyCode from, KeyCode to) {
			if (to < from)
				throw new ArgumentException("Range minimum must be smaller than range maximum. [" + from + "; " + to + "] was given.");
			
			for (KeyCode i = from; i <= to; i++) {
				if (Input.GetKeyDown(i))
					return i;
			}
			return KeyCode.None;
		}

		/// <summary>
		/// Returns the first key in the given range that is is pressed on the keyboard. This uses GetKey.
		/// </summary>
		/// <param name="to">Inclusive.</param>
		/// <returns>KeyCode.None if none is being pressed.</returns>
		public static KeyCode GetFirstKey(KeyCode from, KeyCode to) {
			if (to < from)
				throw new ArgumentException("Range minimum must be smaller than range maximum. [" + from + "; " + to + "] was given.");

			for (KeyCode i = from; i <= to; i++) {
				if (Input.GetKey(i))
					return i;
			}
			return KeyCode.None;
		}

		/// <summary>
		/// Given a list of types (which should be Components), this will return the first instance of one of these components on the given GameObject.
		/// </summary>
		/// <returns>null if no components were found.</returns>
		public static Component GetBehaviourFromObject(Type[] types, GameObject obj)
		{
			foreach (Type type in types)
			{
				Component behavior = obj.GetComponent(type);
				if (behavior != null)
					return behavior;
			}
			return null;
		}
	}
}
