﻿using System.Collections.Generic;
using UnityEngine;
using Game.Ui;
using NaughtyAttributes;

public class NPC : MonoBehaviour
{
	[SerializeField, Multiline]
	private List<string> m_Lines;
	[SerializeField]
	private Color m_ChatColor;

	private int m_CurrentLine = -1;

	private bool m_PlayerInRange;
	private Notification m_TalkNotif;

	private void Update()
	{
		if (m_PlayerInRange && m_CurrentLine != m_Lines.Count - 1)
		{
			if (Input.GetButtonDown("UseItem"))
			{
				// Show next line
				if (m_TalkNotif != null)
				{
					NotificationManager.GetInstance().RemoveNotification(m_TalkNotif);
					m_TalkNotif = null;
				}
				NotificationManager.GetInstance().ShowChatLine(m_Lines[++m_CurrentLine], m_ChatColor);
			}
		}
	}

	private void OnTriggerEnter(Collider other)
	{
		if (m_CurrentLine == -1 && other.tag == "Player")
		{
			if (m_TalkNotif == null)
				m_TalkNotif = NotificationManager.GetInstance().PostNotification(-1.0f, "Press B to talk", Color.white);

			m_PlayerInRange = true;
		}
	}

	private void OnTriggerExit(Collider other)
	{
		if (other.tag == "Player")
		{
			m_PlayerInRange = false;
			if (m_TalkNotif != null)
			{
				NotificationManager.GetInstance().RemoveNotification(m_TalkNotif);
				m_TalkNotif = null;
			}
			NotificationManager.GetInstance().ShowChatLine("", m_ChatColor);
			m_CurrentLine = -1; // Player can restart conversation
		}
	}
}
