﻿using UnityEngine;
using UnityEngine.UI;

namespace Game.Ui {
	public class NotificationManager : MonoBehaviour {
		public string CurrentChatLine {
			get {
				if (m_ChatLine != null) {
					return m_ChatLine.m_Text;
				} else {
					return "";
				}
			}
		}

		[SerializeField]
		private RectTransform m_ChatPosition;
		[SerializeField]
		private GameObject m_NotificationPrefab;

		private Notification m_ChatLine;

		private static NotificationManager s_instance;

		private void Awake() {
			s_instance = this;
		}

		public void ShowChatLine(string text, Color textcolor) {
			if (m_ChatLine != null)
				RemoveNotification(m_ChatLine);
			m_ChatLine = CreateNotification(m_ChatPosition, -1.0f, text, textcolor, new Color(0, 0, 0, 0), new Color(0, 0, 0, 0), 0.0f);
			m_ChatLine.GetComponent<ContentSizeFitter>().horizontalFit = ContentSizeFitter.FitMode.MinSize;
		}

		public Notification PostNotification(float lifetime, string text, Color textcolor) {
			return PostNotification(lifetime, text, textcolor, new Color(0, 0, 0, 0));
		}

		public Notification PostNotification(float lifetime, string text, Color textcolor, Color bgcolor) {
			return PostNotification(lifetime, text, textcolor, bgcolor, new Color(0, 0, 0, 0), 0.0f);
		}

		public Notification PostNotification(float lifetime, string text, Color textcolor, Color bgcolor, Color flashbgcolor, float flashtime) {
			return CreateNotification(transform, lifetime, text, textcolor, bgcolor, flashbgcolor, flashtime);
		}

		public void RemoveNotification(Notification notif) {
            if (notif == null)
                return;

			Destroy(notif.gameObject);
		}

		private Notification CreateNotification(Transform parent, float lifetime, string text, Color textcolor, Color bgcolor, Color flashbgcolor, float flashtime) {
			GameObject tmp = Instantiate(m_NotificationPrefab, parent);
			Notification notif = tmp.GetComponent<Notification>();
			notif.m_Lifetime = lifetime;
			notif.m_Text = text;
			notif.m_Color = textcolor;
			notif.m_BackgroundColor = bgcolor;
			notif.m_BackgroundColorFlash = flashbgcolor;
			notif.m_FlashTime = flashtime;
			notif.m_Flash = !Mathf.Approximately(flashtime, 0.0f);
			return notif;
		}

		public static NotificationManager GetInstance() {
			return s_instance;
		}
	}
}
