﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;

public class ShowCpuLatency : MonoBehaviour {

	PerformanceCounter cpuCounter;
	PerformanceCounter ramCounter;

	Text m_text;
	void Start()
	{
		m_text = GetComponent<Text>();
		cpuCounter = new PerformanceCounter();

		cpuCounter.CategoryName = "Processor";
		cpuCounter.CounterName = "% Processor Time";
		cpuCounter.InstanceName = "_Total";

		ramCounter = new PerformanceCounter("Memory", "Available MBytes");
	}

	void Update()
	{
		m_text.text = getCurrentCpuUsage() + "\n" + getAvailableRAM();
	}

	public string getCurrentCpuUsage()
	{
		return cpuCounter.NextValue() + "%";
	}

	public string getAvailableRAM()
	{
		return ramCounter.NextValue() + "MB";
	}
}
