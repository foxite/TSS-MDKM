﻿using Game.Interfaces;
using UnityEngine;

namespace Game.Ui {
	public class ArmorBar : StatBar {
		[SerializeField]
		private HealthAndArmor m_AttachedEntity;

		protected override void Start() {
			base.Start();
			if (m_AttachedEntity == null)
				Debug.LogError(name + " does not have an entity attached.");
		}

		protected override float GetCurrentStat() {
			return m_AttachedEntity.GetArmor();
		}

		protected override float GetMaxStat() {
			return m_AttachedEntity.GetMaxArmor();
		}
	}
}
