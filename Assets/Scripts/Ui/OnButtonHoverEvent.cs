﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Game.Ui
{
    [RequireComponent(typeof(GraphicRaycaster))]
    public class OnButtonHoverEvent : MonoBehaviour
    {
        GraphicRaycaster m_Raycaster;
        PointerEventData m_PointerEventData;
        EventSystem m_EventSystem;

        void Start()
        {

            m_Raycaster = GetComponent<GraphicRaycaster>();
            m_EventSystem = GetComponent<EventSystem>();
        }

        void Update()
        {

            m_PointerEventData = new PointerEventData(m_EventSystem);
            m_PointerEventData.position = Input.mousePosition;

            List<RaycastResult> results = new List<RaycastResult>();

            m_Raycaster.Raycast(m_PointerEventData, results);

            foreach (RaycastResult result in results)
            {
                if (!result.gameObject.name.Contains("Text"))
                {
					ButtonHighlightManager.Instance.SetHiglightedButton(result.gameObject.GetComponent<Button>());
                }
            }
        }
    }
}
