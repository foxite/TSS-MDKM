﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Game.Character;
using Game.Data;
using Game.Interfaces;
using Game.Ui;

namespace Game.Ui
{

    public class PannelFader : MonoBehaviour
    {
		public static PannelFader Instance { get; private set; }

        private float _timeInSeconds;
        private Image m_fadeImage;

        [SerializeField] private float m_fadeSpeed;


        private void Awake()
        {
			Instance = this;

            m_fadeImage = this.GetComponent<Image>();

            if (PlayerControl.Instance != null)
				PlayerControl.Instance.FreezePlayer(m_fadeSpeed);

            StartCoroutine(FadeOut());
        }

        public void FadeToNextScene(string _nextSceneName, bool _loadGame = false)
        {
            _timeInSeconds = 0;

            if (PlayerControl.Instance != null)
				PlayerControl.Instance.FreezePlayer();

            StartCoroutine(StartFading(_nextSceneName, _loadGame));
        }

        private IEnumerator StartFading(string _sceneName, bool _loadGame)
        {
            while (true)
            {
                _timeInSeconds += Time.deltaTime;
                float _percentageComplete = 1 / m_fadeSpeed * _timeInSeconds;

                m_fadeImage.color = new Color(0, 0, 0, _percentageComplete);

                if (_percentageComplete >= 1) break;
                yield return new WaitForEndOfFrame();
            }
            if (_loadGame)
            {
                SceneLoadAgent agent = GameObject.Find("GameController").AddComponent<SceneLoadAgent>();

                agent.SetCode(() =>
                {
					SaveController.Instance.LoadGame();
					SaveController.Instance.Initialize();

                });
                yield return agent.Load(_sceneName);
            }
            else
            {
                SceneManager.LoadScene(_sceneName);
            }
        }
        private IEnumerator FadeOut()
        {


            while (true)
            {
                _timeInSeconds += Time.deltaTime;
                float _percentageComplete = 1 / m_fadeSpeed * _timeInSeconds;

                float Progress = 1 - _percentageComplete;


                m_fadeImage.color = new Color(0, 0, 0, Progress);

                if (Progress <= 0) break;
                yield return new WaitForEndOfFrame();
            }
        }
    }
}
