﻿using NaughtyAttributes;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Ui
{
	[RequireComponent(typeof(OnButtonHoverEvent))]
	public class ButtonHighlightManager : MonoBehaviour
	{
		public static ButtonHighlightManager Instance { get; private set; }
		[SerializeField] private bool m_useHighligh = true;
		[SerializeField] private List<Button> m_buttons = new List<Button>();
		[SerializeField] private Image m_highlightItem;
		public bool m_debugMode;
		[ShowIf("m_debugMode"), SerializeField] private int m_currentBtn;
		[ShowIf("m_debugMode"), SerializeField] private bool m_isMoved;

		[ShowIf("m_debugMode"), SerializeField] private bool m_isfading;
		[ShowIf("m_debugMode"), SerializeField] private float m_input;

		private void Awake()
		{
			Instance = this;
		}

		public void SetHiglightedButton(Button _button)
		{
			for (int i = 0; i < m_buttons.Count; i++)
			{
				if (m_buttons[i] == _button)
				{
					m_currentBtn = i;
				}

			}
		}

		private void Update()
		{
			if (m_input != 0)
				ControllerSupport();

			if (m_input == 0)
				m_isMoved = false;

			if (m_isfading)
				return;

			if (!m_useHighligh)
				return;

			InputHandler();

			m_highlightItem.transform.position = m_buttons[m_currentBtn].transform.position;


			if (Input.GetButtonDown("Submit"))
			{
				if (m_isfading)
					return;

				m_buttons[m_currentBtn].onClick.Invoke();
			}



		}

		private void InputHandler()
		{
			if (Input.GetAxis("MoveVertical") != 0)
				m_input = Input.GetAxis("MoveVertical");
			else if (Input.GetAxis("DpadVertical") != 0)
				m_input = Input.GetAxis("DpadVertical");
			else
				m_input = 0;
		}

		private void ControllerSupport()
		{

			if (m_input > .3 || Input.GetKeyDown(KeyCode.UpArrow))
			{
				if (!m_isMoved)
				{
					if (m_currentBtn == 0)
					{
						m_isMoved = true;
						m_currentBtn = m_buttons.Count - 1;
						return;
					}
					m_currentBtn--;
				}
				m_isMoved = true;
			}

			if (m_input < -.3 || Input.GetKeyDown(KeyCode.DownArrow))
			{
				if (!m_isMoved)
				{
					if (m_currentBtn == m_buttons.Count - 1)
					{
						m_isMoved = true;
						m_currentBtn = 0;
						return;
					}
					m_currentBtn++;
				}
				m_isMoved = true;
			}
		}
		public void SetFadeStatus(bool _status)
		{
			m_isfading = _status;
		}
		public bool GetFadingStatus()
		{
			return m_isfading;
		}
	}
}
