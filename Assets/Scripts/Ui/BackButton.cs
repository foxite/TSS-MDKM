﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.Ui
{

    public class BackButton : MonoBehaviour {
        public void  SwitchScene(string _sceneName)
        {
            PannelFader.Instance.FadeToNextScene(_sceneName, false);
        }
    }
}
