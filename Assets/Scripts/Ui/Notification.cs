﻿using UnityEngine;
using UnityEngine.UI;
using NaughtyAttributes;

namespace Game.Ui {
	[RequireComponent(typeof(Text))]
	public class Notification : MonoBehaviour {
		public string m_Text;
		public Color m_Color;
		public Color m_BackgroundColor;
		public float m_Lifetime;

		public bool m_Flash;
		[ShowIf("m_Flash")]
		public Color m_BackgroundColorFlash;
		[ShowIf("m_Flash")]
		public float m_FlashTime;

		private float m_FlashtimeRemaining;
		private bool m_Visible;

		protected Text m_TextCmp;
		protected Image m_Background;

		private void Start() {
			m_TextCmp = GetComponent<Text>();
			m_TextCmp.text = m_Text;
			m_TextCmp.color = m_Color;

			m_Background = transform.parent.gameObject.GetComponentInChildren<Image>();
			m_Background.color = m_BackgroundColor;
		}
		
		private void Update() {
			if (!Mathf.Approximately(m_Lifetime, -1.0f)) {
				m_Lifetime -= Time.deltaTime;
				if (m_Lifetime < 0.0f) {
					NotificationManager.GetInstance().RemoveNotification(this);
					Destroy(gameObject);
				}
			}

			if (m_Flash) {
				m_FlashtimeRemaining -= Time.deltaTime;
				if (m_FlashtimeRemaining < 0.0f) {
					m_FlashtimeRemaining = m_FlashTime;
					if (m_Visible) {
						m_Visible = false;
						m_Background.color = new Color(m_BackgroundColor.r,
													   m_BackgroundColor.g,
													   m_BackgroundColor.b,
													   0);
					} else {
						m_Visible = true;
						m_Background.color = m_BackgroundColor;
					}
				}
			}
		}
	}
}
