﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Game.Data;

namespace Game.Ui
{
    public class MainMenuButtonsManager : MonoBehaviour
    {
        public void NewGame(string _SceneName)
        {
			PannelFader.Instance.FadeToNextScene(_SceneName, false);
        }
        public void LoadGame()
		{
			SaveController.Instance.LoadGame();
			PannelFader.Instance.FadeToNextScene(SaveController.Instance.SavedLevelName, true);
			SaveController.Instance.Initialize();
        }


        public void ExitGame()
        {
            Application.Quit();
        }

        public void LoadOptionsScene(string _sceneName)
        {
			PannelFader.Instance.FadeToNextScene(_sceneName, false);
        }
    }
}
