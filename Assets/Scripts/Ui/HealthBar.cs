﻿using Game.Interfaces;
using UnityEngine;

namespace Game.Ui {
	public class HealthBar : StatBar {
		[SerializeField]
		protected HealthAndArmor m_AttachedEntity;

		protected override void Start() {
			if (m_AttachedEntity == null)
				Debug.LogError(name + " does not have an entity attached.");
		}

		protected override float GetCurrentStat() {
			return m_AttachedEntity.GetHealth();
		}

		protected override float GetMaxStat() {
			return m_AttachedEntity.GetMaxHealth();
		}
	}
}
