﻿using Game.Interfaces;
using Game.Inventory;

namespace Game.Ui
{
    public class AmmoBar : StatBar
    {
        protected override void Start()
        {
            base.Start();
        }

        protected override float GetCurrentStat()
        {
            return InventoryManager.Instance.Ammo;
        }

        protected override float GetMaxStat()
        {
            return InventoryManager.Instance.MaxAmmo;
        }
    }
}
