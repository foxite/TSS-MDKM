﻿using Game.Data;
using Game.Pause;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Game.Ui
{
	public enum ButtonEvent
	{
		NewGame,
		LoadMainMenu,
		LoadOptions,
		LoadCredits,
		ExitGame,
		ContinueGame,
		PauseContinue
	}

	[RequireComponent(typeof(Button))]
	public class ButtonOnClickEvent : MonoBehaviour
	{
		[SerializeField] private ButtonEvent m_buttonEvent;
		[SerializeField] private Button m_button;
		[Header("Note: have only 1 object with this on enabled at the time or 2 buttons will be pressed"), SerializeField] private bool m_Add_B_btnSupport;
		

		private void Awake()
		{
			m_button.GetComponent<Button>();

			if (m_button == null)
			{
				Debug.LogError("Please use only the button event script on objects with button component");
				return;
			}
			m_button.onClick.AddListener(() => Load());



		}
		private void Update()
		{
			if (!m_Add_B_btnSupport)
				return;


			if (ButtonHighlightManager.Instance.GetFadingStatus())
				return;
			
			if (Input.GetButtonDown("UseItem"))
			{
				m_button.onClick.Invoke();
			}
		}
		private void Load()
		{
			ButtonHighlightManager.Instance.SetFadeStatus(true);
			switch (m_buttonEvent)
			{
			case ButtonEvent.LoadMainMenu:
				SwitchPauseMenuStatus();
				LoadScene("MainMenu");
				break;

			case ButtonEvent.LoadOptions:
				LoadScene("Options");
				break;

			case ButtonEvent.LoadCredits:
				LoadScene("Credits");
				break;

			case ButtonEvent.ExitGame:
				Application.Quit();
				break;

			case ButtonEvent.ContinueGame:
				SaveController.Instance.LoadGame();
				LoadScene(SaveController.Instance.SavedLevelName, true);
				break;

			case ButtonEvent.PauseContinue:
				SwitchPauseMenuStatus();
				break;
			case ButtonEvent.NewGame:
				LoadScene("Level1");

				break;

			}
		}

		private void SwitchPauseMenuStatus()
		{
			if (PauseManager.Instance == null)
				return;

			ButtonHighlightManager.Instance.SetFadeStatus(false);
			PauseManager.Instance.SwitchPauseStatus();
		}

		private void LoadScene(string _sceneName, bool _loadGame = false)
		{

			if (PannelFader.Instance == null)
			{
				SceneManager.LoadScene(_sceneName);
				ButtonHighlightManager.Instance.SetFadeStatus(false);
			}
			else
			{
				PannelFader.Instance.FadeToNextScene(_sceneName, _loadGame);
			}
		}
	}
}

