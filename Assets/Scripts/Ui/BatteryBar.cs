﻿using Game.flashlight;
using Game.Interfaces;

namespace Game.Ui
{
	public class BatteryBar : StatBar
	{
		private FlashlightManager m_FL;

		protected override void Start()
		{
			base.Start();
			m_FL = FindObjectOfType<FlashlightManager>();
		}

		protected override float GetCurrentStat()
		{
			return m_FL.GetBatteryLifeLeft();
		}

		protected override float GetMaxStat()
		{
			return m_FL.GetMaxBatteryLife();
		}
	}
}
