﻿using Game.Inventory;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Ui
{
    public class GuiManager : MonoBehaviour
    {
		public static GuiManager Instance { get; private set; }

        [SerializeField] private Image[] m_hotbarSprite;
        [SerializeField] private Outline[] m_hotbarOutLines;

        [Space, SerializeField] private Sprite m_invisableSprite;

		private void Awake()
		{
			Instance = this;
		}

		private void Start()
        {
        }

        private void Update()
        {
			ResetHotbarSprites();
        }

        public void ResetHotbarSprites()
        {
            for (int i = 0; i < m_hotbarSprite.Length; i++)
            {
                if (i < InventoryManager.Instance.GetInventoryList().Count)
                    m_hotbarSprite[i].sprite = InventoryManager.Instance.GetInventoryList()[i].m_HotbarIcon;
                else
                    m_hotbarSprite[i].sprite = m_invisableSprite;

            }
            
        }

        /// <summary>
		/// handles what item in the inv is selected and draw an outline on top of it
		/// </summary>
		public void HotbarOutline(int m_activeSlot)
        {
            for (int i = 0; i < InventoryManager.Instance.GetInventoryList().Count; i++)
            {
                if (i == m_activeSlot)
                    m_hotbarOutLines[i].enabled = true;
                else
                    m_hotbarOutLines[i].enabled = false;
            }
        }
    }
}
