﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

// This component should be instantiated by components that need to execute code as soon as the next scene loads.
// It will load a scene, will not destroy itself when the scene is loaded, and then execute the code it was given. It will then destroy itself.
public class SceneLoadAgent : MonoBehaviour
{
    private Action m_codeToRun;
    private void Awake()
    {
        DontDestroyOnLoad(this);
    }

    public void SetCode(Action code)
    {
        m_codeToRun = code;
    }

    public IEnumerator Load(string sceneName)
    {
        AsyncOperation loadOp = SceneManager.LoadSceneAsync(sceneName);
        loadOp.completed += (op) =>
        {
            m_codeToRun();
            Destroy(gameObject);
        };
        yield return null;
    }
}
