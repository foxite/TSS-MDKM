﻿using UnityEngine;
using UnityEngine.UI;

namespace Game.Interfaces {
	[RequireComponent(typeof(Slider))]
	public abstract class StatBar : MonoBehaviour {
		[SerializeField]
		private Slider m_Slider;

		protected virtual void Start() {
			if (m_Slider == null) m_Slider = GetComponent<Slider>(); // This SHOULD* never fail as the component is required (edit: it fucking did)
		}

		private void Update() {
			m_Slider.value = GetCurrentStat() / GetMaxStat() * 100;
		}

		protected abstract float GetCurrentStat();
		protected abstract float GetMaxStat();
	}
}
