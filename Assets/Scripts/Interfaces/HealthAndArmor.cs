﻿using UnityEngine;
namespace Game.Interfaces {
	public abstract class HealthAndArmor : MonoBehaviour {
		public abstract float GetArmor();
		public abstract float GetHealth();
		public abstract void  GiveArmor(float armor);
		public abstract void  GiveHealth(float health);
		public abstract float GetMaxArmor();
		public abstract float GetMaxHealth();
		public abstract void  SetArmor(float armor);
		public abstract void  SetHealth(float health);
		public abstract void  SetMaxArmor(float armor);
		public abstract void  SetMaxHealth(float health);
		public abstract void  DealDamage(float damage);
		// Player might be invincible when they're frozen, for example.
		public abstract bool  IsInvincible();
	}
}
