﻿using System.Collections;
using UnityEngine;
using DG.Tweening;
using Game.Character;
using Game.Ui;
using Game.Inventory;
using Game.Data;
using Game.Interfaces;
using Game.Enemy;
using NaughtyAttributes;

namespace Game.Keycards
{
	public enum DoorType { Hallway, elevator }
	public enum DoorOrientation { Horizontal, Vertical }
	public class AutomaticDoor : MonoBehaviour
	{
		[SerializeField, Space] private float m_activateDistance;
		[SerializeField] private DoorType m_doorType;
		[SerializeField] private DoorOrientation m_doorOrientation;
		[SerializeField] private float m_doorUnlockDuration;
		[SerializeField] private float m_moveDuration;
		[SerializeField] private bool m_hasAudio;
		[Space]
		[SerializeField] private AudioClip m_openClip;
		[SerializeField] private AudioClip m_CloseClip;
		[SerializeField] private AudioClip m_keycardSound;
		[SerializeField] private AudioClip m_wrongKeycardSound;
		[SerializeField] private bool m_doorUnlocked;
		[Space]
		[SerializeField] private KeycardColorCode m_keycardColorCode;
		[SerializeField] private int m_specialID;
		[Space, SerializeField] public bool m_debugMode;
		public bool m_fadeToNextSceneOnOpen;
		[ShowIf("m_fadeToNextSceneOnOpen")]
		[SerializeField] private string m_nextSceneName;
		[ShowIf("m_fadeToNextSceneOnOpen")]
		[SerializeField] private bool m_loadSaveFile;

		private Vector3 m_position;
		private GameObject m_player;
		[ShowIf("m_debugMode"), SerializeField] private bool m_isMoving;
		[ShowIf("m_debugMode"), SerializeField] private bool m_doorClosed = true;
		private Notification m_DoorNotif;
		private AudioSource m_audioSource;
		private int m_KeycardIndex = -1; // Index of the keycard in the player's inventory, if the player is nearby - if he's not, this is -1

		private static EnemyManager[] m_enemies;
		private static bool m_enemiesListFilled = false;

		private void Start()
		{
			if (!m_enemiesListFilled)
			{
				m_enemiesListFilled = true;
				m_enemies = FindObjectsOfType<EnemyManager>();
			}

			m_position = transform.position;
			m_player = PlayerControl.Instance.gameObject;

			if (m_hasAudio)
				m_audioSource = GetComponent<AudioSource>();
		}

		private void OnDestroy()
		{
			m_enemiesListFilled = false;
		}

		private void Update()
		{

			Vector3 _playerHeading = m_player.transform.position - transform.position;
			float _playerDistance = _playerHeading.magnitude;
			
			if (m_debugMode)
				print(_playerDistance);
			if (m_isMoving)
				return;

			if (!m_doorUnlocked)
			{
				if (_playerDistance <= m_activateDistance)
				{
					HandleKeycard(_playerDistance);
				}
			}
			else
			{
				if (m_doorClosed && _playerDistance <= m_activateDistance)
				{
					StartCoroutine(OpenDoor());
				}
				else if (!m_doorClosed && _playerDistance > m_activateDistance)
				{
					StartCoroutine(CloseDoor());
				}
			}

			// Open door for enemies

			// Find nearby closed doors and open them
			foreach (EnemyManager enemy in m_enemies)
			{
				RaycastHit hitInfo;
				Vector3 direction = enemy.transform.position - transform.position;
				if (Physics.Raycast(transform.position, direction, out hitInfo, 1.5f) // Do raycast
					&& hitInfo.transform.CompareTag(Tags.Enemy.ToString())
					&& IsClosed()) // And don't open doors that are already open
				{
					OpenDoorRemotly(2, enemy);
				}
			}
		}

		private void HandleKeycard(float _playerDistance)
		{
			if (_playerDistance < m_activateDistance)
			{
				if (m_doorUnlocked)
				{
					ClearNotification();

				}
				else
				{

					if (InventoryManager.Instance == null)
					{
						print("cant grab inv manager from player");
						return;
					}
					//not performance friendly but it has to be here because  near line 145 depands on it
					m_KeycardIndex = GetKeycard(InventoryManager.Instance);

					if (Input.GetButtonDown("UseItem"))
					{

						if (!m_doorUnlocked)
						{
							if (m_KeycardIndex == -1)
							{
								if (!m_audioSource.isPlaying)
								{
									PlaySound(m_wrongKeycardSound);
									return;
								}
								return;
							}

							if (m_keycardSound == null)
							{
								InventoryManager.Instance.RemoveItem(m_KeycardIndex);
								m_doorUnlocked = true;
							}
							else
							{
								StartCoroutine(PlayKeycardSfx(InventoryManager.Instance));
							}

						}

					}

					ClearNotification();
					if (m_KeycardIndex == -1)
					{
						if (m_doorType == DoorType.Hallway)
							m_DoorNotif = NotificationManager.GetInstance().PostNotification(1f, "You need a keycard to open this door", Color.white);
						else if(m_doorType == DoorType.elevator)
							m_DoorNotif = NotificationManager.GetInstance().PostNotification(1f, "You need a keycard to use this elevator", Color.white);
					}
					else
					{
						if (m_doorType == DoorType.Hallway)
							m_DoorNotif = NotificationManager.GetInstance().PostNotification(1.0f, "Press B to open the door", Color.white);
						else if (m_doorType == DoorType.elevator)
							m_DoorNotif = NotificationManager.GetInstance().PostNotification(1.0f, "Press B to unlock the elevator", Color.white);
					}

				}
			}
		}

		private void ClearNotification()
		{
			if (m_DoorNotif != null)
			{
				NotificationManager.GetInstance().RemoveNotification(m_DoorNotif);
				m_DoorNotif = null;
			}
		}

		private IEnumerator PlayKeycardSfx(InventoryManager _inventoryManager)
		{
			if (!m_audioSource.isPlaying)
			{
				PlaySound(m_keycardSound);
				yield return new WaitForSeconds(m_keycardSound.length);
				_inventoryManager.RemoveItem(m_KeycardIndex);
				m_doorUnlocked = true;
			}
		}

		private void PlaySound(AudioClip _clip)
		{
			m_audioSource.clip = _clip;
			m_audioSource.Play();
		}

		private IEnumerator OpenDoor()
		{
			if (!m_doorClosed)
				yield return null;
			if (m_hasAudio)
			{
				PlaySound(m_openClip);

			}
			m_isMoving = true;
			m_doorClosed = false;

			if (m_doorType == DoorType.Hallway)
			{

				if (m_hasAudio)
				{
					PlaySound(m_openClip);

				}
				if (m_doorOrientation == DoorOrientation.Horizontal)
				{
					transform.DOMoveZ(m_position.z + .15f, m_doorUnlockDuration);
					yield return new WaitForSeconds(m_doorUnlockDuration);
					transform.DOMoveX(m_position.x + .9f, m_moveDuration);
					yield return new WaitForSeconds(m_moveDuration);
				}
				else if (m_doorOrientation == DoorOrientation.Vertical)
				{
					transform.DOMoveX(m_position.x + .15f, m_doorUnlockDuration);
					yield return new WaitForSeconds(m_doorUnlockDuration);
					transform.DOMoveZ(m_position.z + 1f, m_moveDuration);
					FadeToNextScene();
					yield return new WaitForSeconds(m_moveDuration);
				}
				m_isMoving = false;


			}
			else if (m_doorType == DoorType.elevator)
			{
				if (m_doorOrientation == DoorOrientation.Horizontal)
				{

					yield return new WaitForSeconds(m_doorUnlockDuration);
					transform.DOMoveX(m_position.x + 1.1f, m_moveDuration);
					FadeToNextScene();
					yield return new WaitForSeconds(m_moveDuration);
				}
				else if (m_doorOrientation == DoorOrientation.Vertical)
				{
					yield return new WaitForSeconds(m_doorUnlockDuration);
					transform.DOMoveZ(m_position.z + 1.1f, m_moveDuration);
					FadeToNextScene();
					yield return new WaitForSeconds(m_moveDuration);
				}
				m_isMoving = false;
			}
		}
		/// <summary>
		/// opens the door for ... seconds
		/// </summary>
		public void OpenDoorRemotly(float _openDuration, EnemyManager _sender)
		{
			if (m_isMoving)
				return;
			if (m_doorClosed && m_doorUnlocked)
				StartCoroutine(OpenRemotly(_openDuration, _sender));
		}
		IEnumerator OpenRemotly(float _openDuration, EnemyManager _sender)
		{
			StartCoroutine(_sender.Wait(1));
			StartCoroutine(OpenDoor());
			yield return new WaitForSeconds(_openDuration);
			StartCoroutine(CloseDoor());
		}
		private IEnumerator CloseDoor()
		{
			if (m_doorClosed)
				yield return null;

			m_isMoving = true;
			m_doorClosed = true;
			if (m_hasAudio)
			{
				PlaySound(m_CloseClip);
			}
			if (m_doorType == DoorType.Hallway)
			{


				if (m_doorOrientation == DoorOrientation.Horizontal)
				{

					transform.DOMoveX(m_position.x, m_moveDuration);
					yield return new WaitForSeconds(m_moveDuration);
					transform.DOMoveZ(m_position.z, m_doorUnlockDuration);
					yield return new WaitForSeconds(m_doorUnlockDuration);
				}
				else if (m_doorOrientation == DoorOrientation.Vertical)
				{
					transform.DOMoveZ(m_position.z, m_moveDuration);
					yield return new WaitForSeconds(m_moveDuration);
					transform.DOMoveX(m_position.x, m_doorUnlockDuration);
					yield return new WaitForSeconds(m_doorUnlockDuration);
				}
				m_isMoving = false;
			}
			else if (m_doorType == DoorType.elevator)
			{
				if (m_doorOrientation == DoorOrientation.Horizontal)
				{
					transform.DOMoveX(m_position.x, m_moveDuration);
					yield return new WaitForSeconds(m_moveDuration);
				}
				else if (m_doorOrientation == DoorOrientation.Horizontal)
				{
					transform.DOMoveZ(m_position.z, m_moveDuration);
					yield return new WaitForSeconds(m_moveDuration);
				}
				m_isMoving = false;
			}
		}
		/// <summary>
		/// Checks if the given InventoryManager has the correct keycard to open this door, and then returns the index of that item.
		/// </summary>
		/// <param name="obj"></param>
		/// <returns>The index of the keycard, -1 if none found</returns>
		private int GetKeycard(InventoryManager inv)
		{
			for (int i = 0; i < inv.GetInventoryList().Count; i++)
			{
				if (inv.GetInventoryList()[i] == null)
					break;
				if (inv.GetInventoryList()[i].m_ItemType == ItemType.keycard)
				{
					Keycard _keycard = inv.GetInventoryList()[i].gameObject.GetComponent<Keycard>();

					if (_keycard.m_keycardColorCode == KeycardColorCode.AllDoorKey)
						return i;

					if ((_keycard.m_keycardColorCode == m_keycardColorCode && _keycard.m_specialID == m_specialID))
					{
						return i;
					}
				}
			}

			return -1;
		}
		private void FadeToNextScene()
		{

			if (m_fadeToNextSceneOnOpen)
			{
				if (PannelFader.Instance == null)
				{
					Debug.LogError("Please enable the fade pannel gameobject in canvas to switch to the new scene");
					return;
				}

				SaveController.Instance.SaveGame(m_nextSceneName,
					PlayerControl.Instance.GetComponent<HealthAndArmor>(),
					InventoryManager.Instance);

				PannelFader.Instance.FadeToNextScene(m_nextSceneName, m_loadSaveFile);
			}
		}

		public bool IsClosed() => m_doorClosed;
	}
}
