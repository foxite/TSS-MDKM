﻿// Behold, the tinyiest component ever
namespace Game.Keycards {
	public class Keycard : UnityEngine.MonoBehaviour {
		public KeycardColorCode m_keycardColorCode;
		public int m_specialID;
	}

	public enum KeycardColorCode { red, green, blue, purple, AllDoorKey }

	public static class KeycardColorCodeFunctions {
		public static KeycardColorCode FromString(string str) {
			switch (str) {
			case "red":        return KeycardColorCode.red;
			case "green":      return KeycardColorCode.green;
			case "blue":       return KeycardColorCode.blue;
			case "purple":     return KeycardColorCode.purple;
			case "AllDoorKey": return KeycardColorCode.AllDoorKey;
			default:           throw new System.ArgumentException(str + " is not a valid KeycardColorCode.");
			}
		}
	}
}
