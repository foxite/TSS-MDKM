﻿using System.Collections;
using System.Collections.Generic;
using Game.Character;
using Game.Data;
using Game.Interfaces;
using Game.Keycards;
using Game.Util;
using Game.weapons;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.AI;

namespace Game.Enemy
{
	public enum EnemyState { Idle, Roaming, RoamingIdle, WalkingTowardsPlayer, RunningTowardsPlayer, Attacking, MoveAttacking, shootToPlayer, walkTowardsLastKnownPlayerLoc, death }
	[RequireComponent(typeof(NavMeshAgent))]
	public class EnemyManager : HealthAndArmor
	{
		#region varibles

		[SerializeField] private int m_scoreToGiveOnDeath;

		[BoxGroup("Movement"), SerializeField] private EnemyState m_startState;
		[BoxGroup("Movement")] public bool m_roamAround;

		[BoxGroup("Movement"), ShowIf("m_roamAround"), SerializeField] private int m_roomNumber;
		[BoxGroup("Movement"), ShowIf("m_roamAround")] public bool m_idleSometime;
		[BoxGroup("Movement"), ShowIf("m_idleSometime"), SerializeField] private float m_idleDuration;
		[BoxGroup("Movement"), ShowIf("m_idleSometime"), SerializeField, Range(0, 10)] private float m_idlechance;
		[BoxGroup("Movement"), ShowIf("m_idleSometime"), SerializeField] private bool m_idleAfterRoam;

		[BoxGroup("Movement"), SerializeField] private float m_walkSpeed, m_sprintSpeed;
		[BoxGroup("Movement"), SerializeField] private bool m_sprintTowardsPlayer;

		[BoxGroup("Field of view"), SerializeField, Range(10, 100)] private float m_fov;
		[BoxGroup("Field of view"), SerializeField] private float m_viewRange, m_hearingRange;

		[BoxGroup("Weapon"), Header("Note: has infinite ammo")] public bool m_hasGun;
		[BoxGroup("Weapon"), SerializeField] private float m_attackCooldown;

		[InfoBox ("Ranged attributes are not used if this enemy does not have a gun.", "HasNoGun")]
		[BoxGroup("Ranged"), ShowIf("m_hasGun"), SerializeField] private int m_bulletInMag;
		[BoxGroup("Ranged"), ShowIf("m_hasGun"), SerializeField] private float m_reloadSpeed, m_roundsPerMinut;
		[BoxGroup("Ranged"), ShowIf("m_hasGun"), SerializeField] private GameObject m_bullet;
		[BoxGroup("Ranged"), ShowIf("m_hasGun"), SerializeField] private Transform m_bulletSpawningPos;
		[BoxGroup("Ranged"), ShowIf("m_hasGun"), SerializeField] private AudioClip m_shotSound;

		[InfoBox ("Melee attributes are not used if this enemy has a gun.", "HasGun")]
		[BoxGroup("Melee"), HideIf("m_hasGun"), SerializeField] private float m_attackDamage; // This one must be seperate because of the infobox
		[BoxGroup("Melee"), HideIf("m_hasGun"), SerializeField] private float m_attackRange, m_criticalHitChance, m_criticalAttackDamage;

		[BoxGroup("Health"), SerializeField] private float m_maxHealth, m_maxArmor, m_bulletReceivingDamage;

		[BoxGroup("sfx"), SerializeField] private AudioClip m_getHitSfx, m_DeadSfx;
		[BoxGroup("sfx"), SerializeField] private AudioClip[] m_FootstepSounds;

		[BoxGroup("Debug")] public bool m_debugMode;
		[BoxGroup("Debug"), ShowIf("m_debugMode")] public bool m_roamingDebugMarker;

		[BoxGroup("Debug"), ShowIf("m_debugMode"), ReadOnly, SerializeField] private EnemyState m_currentState;
		[BoxGroup("Debug"), ShowIf("m_debugMode"), ReadOnly, SerializeField] private int m_currentBulletInMag;
		[BoxGroup("Debug"), ShowIf("m_debugMode"), ReadOnly, SerializeField] private float m_idleCounter, m_shootTimer, m_rps, m_reloadTimer, m_currentHealth, m_currentArmor;

		[BoxGroup("Debug"), ShowIf("m_debugMode"), ReadOnly, SerializeField] private bool m_isRoaming, m_isRoamingIdle, m_ignorePlayer;
		[BoxGroup("Debug"), ShowIf("m_debugMode"), ReadOnly, SerializeField] private Color m_viewRangeGizmoColor, m_attackRangeGizmoColor, m_hearingRangeGizmoColor;
		private PlayerControl m_player;
		private Vector3 m_currentAiDestination;
		private List<Ray> m_fieldOfViewRays = new List<Ray>();
		private Vector3 m_leftRayDirection, m_rightRayDirection;
		private List<GameObject> m_floorTiles = new List<GameObject>();
		private NavMeshAgent m_agent;
		private AudioSource m_audioSource;
		private AudioSource m_footStepAudioSource;
		private bool m_sawPlayerAndShootAtIt;
		private bool m_hasEndedDeadSequence;
		private float m_StepCycle;
		private float m_NextStep;
		private float m_StepInterval;
		private float m_attackTimer;
		private bool m_isWaiting = false;
		#endregion

		private void Start()
		{
			InitializeComponents();
			AddPathfindingFloor();
			SetEnemyState(m_startState);
		}

		/// <summary>
		/// add all avalible floors tho the ai pathfinding
		/// </summary>
		private void AddPathfindingFloor()
		{
			foreach (GameObject _floor in GameObject.FindGameObjectsWithTag("Room" + m_roomNumber + "Floor"))
			{
				m_floorTiles.Add(_floor);
			}
		}
		/// <summary>
		/// change the enemy state
		/// </summary>
		/// <param name="_state">new enemy state</param>
		public void SetEnemyState(EnemyState _state)
		{
			m_currentState = _state;
		}
		/// <summary>
		/// sets the private unity components and starting values of this enemy
		/// </summary>
		private void InitializeComponents()
		{
			m_agent = GetComponent<NavMeshAgent>();
			m_player = PlayerControl.Instance;
			m_audioSource = GetComponents<AudioSource>()[0];
			m_footStepAudioSource = GetComponents<AudioSource>()[1];
			m_rps = 1f / (m_roundsPerMinut / 60f);
			m_currentBulletInMag = m_bulletInMag;
			m_currentState = m_startState;
			m_currentHealth = m_maxHealth;
			m_currentArmor = m_maxArmor;
			m_agent.speed = m_walkSpeed;
		}

		private void Update()
		{
			if (m_isWaiting)
				return;

			HealthManager();//this can set the state to dead so thats why the check is under this 

			if (m_currentState == EnemyState.death)
				return;

			if (PlayerControl.Instance.m_actionState == PlayerActionState.Dead)
				return;

			ProgressStepCycle(m_agent.speed);
			CheckIfEnemyReachedDestination();
			RaycastManager();
			ShootManager();
			AttackPlayer();
			Hearing();
			GoRoamIdle();
			GoRoamAround();
			RoamingManager();
			CheckIfCanAttack();

			if (m_currentState != EnemyState.shootToPlayer)
				if (m_sawPlayerAndShootAtIt)
				{
					SetAiDestination(m_player.transform.position);
					m_currentState = EnemyState.walkTowardsLastKnownPlayerLoc;
					m_sawPlayerAndShootAtIt = false;
				}
		}

		/// <summary>
		/// handles the shooting
		/// </summary>
		private void ShootManager()
		{
			if (m_currentState == EnemyState.shootToPlayer)
			{
				m_shootTimer += Time.deltaTime;

				if (m_currentBulletInMag > 0 && m_shootTimer > m_rps)
				{
					SetAiDestination(this.transform.position);
					transform.LookAt(m_player.transform.position);
					m_shootTimer = 0;
					m_currentBulletInMag--;
					GameObject _ga = Instantiate(m_bullet, m_bulletSpawningPos.position, transform.localRotation);
					_ga.transform.localRotation = this.gameObject.transform.localRotation;

					_ga.GetComponent<Bullet>().m_owner = this.gameObject;

					AudioSource _as = _ga.GetComponent<AudioSource>();
					_as.clip = m_shotSound;
					_as.Play();
					m_currentState = EnemyState.Idle;
				}
				else
				{
					m_reloadTimer += Time.deltaTime;
					if (m_reloadTimer >= m_reloadSpeed)
					{
						m_reloadTimer = 0;
						m_currentBulletInMag = m_bulletInMag;
					}
				}
			}
		}

		/// <summary>
		/// manage if the enemy goes to roam idle or roam arround 
		/// </summary>
		private void RoamingManager()
		{
			if (!m_roamAround)
			{
				if (m_currentState == EnemyState.Roaming)
				{
					// m_roamAround may have been changed externally, in which case we should stop roaming.
					m_currentState = EnemyState.Idle;
					SetAiDestination(transform.position);
				}
				return;
			}

			if (m_isRoaming || m_isRoamingIdle)
				return;

			if (m_currentState == EnemyState.Idle)
			{



				if (m_idleSometime)
				{
					float _rngNumber = UnityEngine.Random.Range(0, 10);

					if (_rngNumber <= m_idlechance)
						m_currentState = EnemyState.RoamingIdle;
					else
						m_currentState = EnemyState.Roaming;
				}
				else
				{
					m_currentState = EnemyState.Roaming;
				}
			}
		}

		/// <summary>
		/// let the enemy roam arround
		/// </summary>
		private void GoRoamIdle()
		{
			if (m_isRoamingIdle)
				return;

			if (m_currentState != EnemyState.RoamingIdle)
				return;

			m_isRoamingIdle = true;
			StartCoroutine(IdleTimeCounter());

		}

		/// <summary>
		/// a local thread timer that sets the enemy idle state back to normal if the timer has been passed
		/// 
		/// 
		/// is that good english?
		/// 
		/// or is it even a sentence
		/// </summary>
		/// <returns></returns>
		private IEnumerator IdleTimeCounter()
		{
			while (true)
			{
				m_idleCounter += Time.deltaTime;
				if (m_idleCounter >= m_idleDuration)
				{
					break;
				}
				yield return new WaitForEndOfFrame();
			}

			m_idleCounter = 0;
			m_isRoamingIdle = false;
			m_currentState = EnemyState.Idle;
		}

		/// <summary>
		/// check if the enemy reached the destination, if so go back to idle
		/// </summary>
		private void CheckIfEnemyReachedDestination()
		{
			if (CheckIfPathComplete(m_currentAiDestination))
			{
				if (m_idleAfterRoam)
				{
					m_isRoamingIdle = false;
					m_isRoaming = false;
					m_currentState = EnemyState.RoamingIdle;
				}
				else
				{
					m_isRoamingIdle = false;
					m_isRoaming = false;
					m_currentState = EnemyState.Idle;
				}
			}
		}

		/// <summary>
		/// enemy start roaming arround and return true when reached destination
		/// </summary>
		/// <returns>destination reached</returns>
		private void GoRoamAround()
		{
			if (m_currentState != EnemyState.Roaming)
				return;
			if (m_isRoaming)
				return;

			m_isRoaming = true;

			m_currentAiDestination = CalculateNextRoamingDestination();

			if (CheckPathIsReachable())
			{
				SetAiDestination();
			}
			else
			{
				m_isRoaming = false;
			}
		}

		/// <summary>
		/// set the ai destination to de destination from the global varible(m_currentaiDestinsation)
		/// </summary>
		private void SetAiDestination()
		{
			Vector3 _newDest = new Vector3(m_currentAiDestination.x, transform.position.y, m_currentAiDestination.z);
			DrawDebugMarker(_newDest);
			m_agent.SetDestination(_newDest);
		}
		
		/// <summary>
		/// Set the AI destination. If the position is not on the navmesh, it will try to go to the closest point (with an optional maximum distance).
		/// </summary>
		/// <param name="maxDistance">If the closest point is further away than this, it will not try to go there.</param>
		private void SetAiDestination(Vector3 _nextDestination, float maxDistance = 2.0f)
		{
			m_currentAiDestination = _nextDestination;
			DrawDebugMarker(_nextDestination);

			if (!m_agent.SetDestination(_nextDestination))
			{
				NavMeshHit hit;
				if (NavMesh.SamplePosition(_nextDestination, out hit, maxDistance, m_agent.areaMask))
				{
					m_agent.SetDestination(hit.position);
				}
			}
		}
		/// <summary>
		/// draws a debug marker at the given destination
		/// </summary>
		/// <param name="_newDest">vector 3 position</param>
		private void DrawDebugMarker(Vector3 _newDest)
		{
			if (m_roamingDebugMarker)
			{
				GameObject _marker = GameObject.CreatePrimitive(PrimitiveType.Cube);
				_marker.GetComponent<Renderer>().material.color = Color.green;
				_marker.transform.position = _newDest;
				_marker.GetComponent<Collider>().enabled = false;
				_marker.transform.localScale = new Vector3(.5f, .5f, .5f);
			}
		}

		/// <summary>
		/// returns if the enemy reached the destination, and automaticly set the state back to idle
		/// </summary>
		/// <param name="_destination">vector 3 pos of the destination</param>
		/// <returns></returns>
		private bool CheckIfPathComplete(Vector3 _destination)
		{
			if (!m_isRoaming)
				return false;

			if (Vector3.Distance(transform.position, _destination) < 1 && Vector3.Distance(transform.position, _destination) > .1f)
			{
				m_isRoaming = false;
				m_currentState = EnemyState.Idle;
				return true;
			}
			return false;
		}

		/// <summary>
		/// Returns wether the current AI destination is reachable.
		/// </summary>
		/// <returns>just that</returns>
		private bool CheckPathIsReachable()
		{
			NavMeshPath _path = new NavMeshPath();
			m_agent.CalculatePath(m_currentAiDestination, _path);
			return _path.status == NavMeshPathStatus.PathComplete;
		}
		
		/// <summary>
		/// Calculate a vector 3 position in the range of the user selected range-
		/// </summary>
		private Vector3 CalculateNextRoamingDestination()
		{
			if (m_currentState != EnemyState.Roaming)
				return transform.position;

			int _random = Random.Range(0, m_floorTiles.Count);
			Vector3 _randomPos = m_floorTiles[_random].transform.position;

			return _randomPos;
		}

		/// <summary>
		/// draw roam randius in editor
		/// </summary>
		private void OnDrawGizmos()
		{
#if (UNITY_EDITOR)
			if (!Application.isEditor)
				return;

			if (m_debugMode)
			{
				if (!m_hasGun)
				{
					UnityEditor.Handles.color = m_attackRangeGizmoColor;
					UnityEditor.Handles.DrawWireDisc(this.transform.position, Vector3.up, m_attackRange);
				}
				//fov
				UnityEditor.Handles.color = m_hearingRangeGizmoColor;
				UnityEditor.Handles.DrawWireDisc(this.transform.position, Vector3.up, m_hearingRange);

				DrawFieldOfViewGizmos();
			}
#endif
		}
		/// <summary>
		/// draws in editor only field of view circle arround enemy
		/// </summary>
		private void DrawFieldOfViewGizmos()
		{
#if (UNITY_EDITOR)
			UnityEditor.Handles.color = m_viewRangeGizmoColor;
			UnityEditor.Handles.DrawWireDisc(this.transform.position, Vector3.up, m_viewRange);

			Quaternion leftRayRotation = Quaternion.AngleAxis(-m_fov / 2, Vector3.up);
			Quaternion rightRayRotation = Quaternion.AngleAxis(m_fov / 2, Vector3.up);

			m_leftRayDirection = leftRayRotation * transform.forward;
			m_rightRayDirection = rightRayRotation * transform.forward;

			Gizmos.DrawRay(transform.position, m_leftRayDirection * m_viewRange);
			Gizmos.DrawRay(transform.position, m_rightRayDirection * m_viewRange);
#endif
		}

		/// <summary>
		/// raycast enemy line of sight
		/// </summary>
		private void RaycastManager()
		{
			if (m_ignorePlayer)
				return;

			// Player raycast
			Vector3 direction = m_player.transform.position - transform.position;
			RaycastHit hitInfo;
			if (Physics.Raycast(transform.position, direction, out hitInfo, m_viewRange) // Do raycast
				&& Mathf.Abs(Vector3.Angle(transform.forward, direction)) < m_fov / 2 // Check if hit is within fov
				&& hitInfo.transform.CompareTag(Tags.Player.ToString()))
			{
				// Saw player
				if (m_hasGun)
				{
					m_currentState = EnemyState.shootToPlayer;
					m_sawPlayerAndShootAtIt = true;
				}
				else
				{
					RunTowardsPlayer(hitInfo);
					print("saw player and gonne attack him");
				}
			}
		}

		/// <summary>
		/// freezes the enemy for ... seconds and then continue with his destination.
		/// </summary>
		/// <param name="_duration">waiting time in seconds</param>
		/// <returns></returns>
		public IEnumerator Wait(float _duration)
		{
			Vector3 _tmpPos = GetDestination();
			m_isWaiting = true;
			m_agent.enabled = false;
			yield return new WaitForSeconds(_duration);
			m_agent.enabled = true;
			m_isWaiting = false;
			SetAiDestination(_tmpPos);

		}

		/// <summary>
		/// sets the ai speed and destination to the player
		/// </summary>
		/// <param name="_hit">raycast hit for destination</param>
		private void RunTowardsPlayer(RaycastHit _hit)
		{
			if (m_sprintTowardsPlayer)
			{
				m_currentState = EnemyState.RunningTowardsPlayer;
				SetSpeed(m_sprintSpeed);
				SetAiDestination(_hit.transform.position);
			}
			else
			{
				SetAiDestination(_hit.transform.position);
				m_currentState = EnemyState.WalkingTowardsPlayer;
				SetSpeed(m_walkSpeed);
			}
		}

		/// <summary>
		/// sets the enemy speed to parameter
		/// </summary>
		/// <param name="_walkSpeed">float speed</param>
		private void SetSpeed(float _walkSpeed)
		{
			m_agent.speed = _walkSpeed;
		}

		/// <summary>
		/// sets the state to attacking if player is in range
		/// </summary>
		private void CheckIfCanAttack()
		{
			if (m_hasGun)
				return;
			if (m_currentState != EnemyState.Attacking && m_currentState != EnemyState.death)
			{

				if (Vector3.Distance(transform.position, m_player.transform.position) < m_attackRange)
				{
					m_currentState = EnemyState.Attacking;
				}
			}
		}
		/// <summary>
		/// attack the player
		/// </summary>
		private void AttackPlayer()
		{
			if (m_currentState == EnemyState.Attacking)
			{
				SetAiDestination(transform.position);
				m_attackTimer += Time.deltaTime;

				if (m_attackTimer >= m_attackCooldown)
				{
					m_attackTimer = 0;
					m_currentState = EnemyState.Idle;
					m_player.DealDamage(Random.value < m_criticalHitChance ? m_criticalAttackDamage : m_attackDamage);
				}
			}
		}

		public Vector3 GetDestination()
		{
			return m_currentAiDestination;
		}

		public NavMeshAgent GetAgent()
		{
			return m_agent;
		}
		/// <summary>
		/// handles the enemy's ears
		/// </summary>
		private void Hearing()
		{
			float _distance = Vector3.Distance(new Vector3(transform.position.x, 0, transform.position.z),
				new Vector3(m_player.transform.position.x, 0, m_player.transform.position.z));
			if (_distance < m_hearingRange)
			{
				transform.LookAt(m_player.transform.position);
				if (m_hasGun)
					m_currentState = EnemyState.shootToPlayer;
				else
				{
					if (m_sprintTowardsPlayer)
						m_currentState = EnemyState.RunningTowardsPlayer;
					else
						m_currentState = EnemyState.WalkingTowardsPlayer;
				}
			}
		}

		/// <summary>
		/// handles the death of the enemy
		/// </summary>
		private void HealthManager()
		{
			if (m_currentHealth <= 0)
			{
				if (m_hasEndedDeadSequence)
					return;

				m_currentState = EnemyState.death;
				DataManager.Instance.m_enemysKilled++;
				DataManager.Instance.m_score += m_scoreToGiveOnDeath;
				SetAiDestination(transform.position);
				m_hasEndedDeadSequence = true;
				SetAiDestination(transform.position);
				PlaySfx(m_DeadSfx);
				return;
			}
		}

		private void OnTriggerEnter(Collider other)
		{

			if (!CheckBulletOwner(other.gameObject))
				return;

			PlaySfx(m_getHitSfx);
			DealDamage(m_bulletReceivingDamage);

		}

		/// <summary>
		/// checks if the bullet(collided item) isnt his own
		/// </summary>
		/// <param name="_bulletGm"></param>
		/// <returns></returns>
		private bool CheckBulletOwner(GameObject _bulletGm)
		{

			if (m_currentState == EnemyState.death)
				return false;

			Bullet _bullet = _bulletGm.GetComponent<Bullet>();

			if (_bullet == null)
				return false;

			if (_bullet.m_owner == this.gameObject)
				return false;

			if (!_bullet.m_isAlive)
				return false;

			return true;
		}

		/// <summary>
		/// plays a audio clip
		/// </summary>
		/// <param name="_sfx">audio clip</param>
		private void PlaySfx(AudioClip _sfx)
		{

			if (m_audioSource == null)
				return;

			m_audioSource.clip = _sfx;
			m_audioSource.Play();

		}

		/// <summary>
		/// returns the current health
		/// </summary>
		/// <returns></returns>
		public override float GetHealth()
		{
			return m_currentHealth;
		}

		/// <summary>
		/// returens max health
		/// </summary>
		/// <returns></returns>
		public override float GetMaxHealth()
		{
			return m_maxHealth;
		}

		/// <summary>
		/// returns current armor count
		/// </summary>
		/// <returns></returns>
		public override float GetArmor()
		{
			return m_currentArmor;
		}

		/// <summary>
		/// returns max armor
		/// </summary>
		/// <returns></returns>
		public override float GetMaxArmor()
		{
			return m_maxArmor;
		}


		/// <summary>
		/// give ... armor to enemy
		/// </summary>
		/// <param name="armor"></param>
		public override void GiveArmor(float armor)
		{
			m_currentArmor += armor;
		}


		/// <summary>
		/// give ... health to enemy
		/// </summary>
		/// <param name="health"> ammount</param>
		public override void GiveHealth(float health)
		{
			m_currentHealth += health;
		}


		/// <summary>
		/// do damage against this object
		/// </summary>
		/// <param name="damage"></param>
		public override void DealDamage(float damage)
		{
			m_currentArmor -= damage;
			if (m_currentArmor <= 0)
			{
				m_currentHealth += m_currentArmor; // m_armor is negative so adding it to m_health decreases m_health
				m_currentArmor = 0;
			}
		}

		/// <summary>
		/// force setting the armor to ...
		/// </summary>
		/// <param name="armor"></param>
		public override void SetArmor(float armor)
		{
			m_currentArmor = armor;
		}

		/// <summary>
		/// force setting the health
		/// </summary>
		/// <param name="health"></param>
		public override void SetHealth(float health)
		{
			m_currentHealth = health;
		}

		/// <summary>
		/// force setting the max armor
		/// </summary>
		/// <param name="armor"></param>

		public override void SetMaxArmor(float armor)
		{
			m_maxArmor = armor;
		}

		/// <summary>
		/// force setting max health
		/// </summary>
		/// <param name="health"></param>
		public override void SetMaxHealth(float health)
		{
			m_maxHealth = health;
		}


		/// <summary>
		/// returns if the enemy is invincible
		/// </summary>
		/// <returns></returns>
		public override bool IsInvincible()
		{
			return false;
		}

		/// <summary>
		/// handles footstep sounds
		/// </summary>
		/// <param name="speed"></param>
		private void ProgressStepCycle(float speed)
		{
			//thanks unity standart player script
			if (m_agent.velocity.sqrMagnitude > 0)
			{
				m_StepCycle += (m_agent.velocity.magnitude + speed) * Time.fixedDeltaTime;
			}

			if (m_StepCycle <= m_NextStep)
			{
				return;
			}

			m_NextStep = m_StepCycle + m_StepInterval;

			PlayFootStepAudio();
		}

		/// <summary>
		/// plays a footstep sound, this is based on the progresstepcycle method
		/// </summary>
		private void PlayFootStepAudio()
		{
			//thanks unity
			if (!m_footStepAudioSource.isPlaying)
			{
				int n = UnityEngine.Random.Range(1, m_FootstepSounds.Length);
				m_footStepAudioSource.clip = m_FootstepSounds[n];
				m_footStepAudioSource.PlayOneShot(m_footStepAudioSource.clip);
				m_FootstepSounds[n] = m_FootstepSounds[0];
				m_FootstepSounds[0] = m_footStepAudioSource.clip;
			}
		}


		// These are used for meta attributes in the weapon properties
		private bool HasGun() => m_hasGun;
		private bool HasNoGun() => !m_hasGun;
	}
}
