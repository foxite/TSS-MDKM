﻿using UnityEngine;
using Game.Inventory;
using Game.Character;

namespace Game.cheats
{
    public class CheatManager : MonoBehaviour
    {
		[SerializeField] private InventoryItem m_CheatKeyCard;
        void Update()
        {
			// Give all ammo types
            if (Input.GetKeyDown(KeyCode.F1))
            {
				InventoryManager.Instance.AddBatteries(10000);
				InventoryManager.Instance.AddAmmo(10000);
            }
			// Restore health/armor
            if (Input.GetKeyDown(KeyCode.F2))
            {
                PlayerControl.Instance.GiveHealth(100);
				PlayerControl.Instance.GiveArmor(100);
            }
			// Give all-access keycard
			if (Input.GetKeyDown(KeyCode.F3)) {
				InventoryManager.Instance.AddItem(m_CheatKeyCard);
			}
        }
    }
}
